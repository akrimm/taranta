
WebJive Architecture
####################

WebJive is composed of four projects: 

#. **WebJive Frontend**  https://gitlab.com/MaxIV/webjive

#. **WebJive Dashboards**  https://gitlab.com/MaxIV/dashboard-repo

#. **WebJive Auth**  https://gitlab.com/MaxIV/webjive-auth

#. **TangoGQL**  https://gitlab.com/MaxIV/web-maxiv-tangogql


The following diagram shows an overview of the architecture: 

.. figure:: _static/img/WebJive_Architecure_1.png
   :width: 100%
   :alt: Block diagram of the architecture


WebJive Frontend is a React_  client that is used both to browse, inspect and control Tango devices and to create and run dashboards, each composed of  widgets. WebJive Frontend accesses the Tango Control Framework through **TangoGql**, a GraphQL_ interface to Tango. The communication between WebJive and TangoGQL is managed by an appropriate frontend component. 

.. _React: https://reactjs.org/
.. _GraphQL: https://graphql.org/

To use WebJive, an anonymous user can run dashboards and can browse and inspect devices. However, to be able to send commands to devices, to change their attributes and to create or modify dashboards, you need to authenticate with WebJive. WebJive uses **webjive-auth**  to manage users; it  accesses an LDAP repository or a  JSON file to retrieve user information. 

The dashboards created by an authenticated user are stored in a MongoDB_  database through the **dashboard-repo**  application. 

.. _MongoDB: https://www.mongodb.com/



