Attribute Writer Dropdown Widget
**********************************

The widget enables the user to write predefined values to an attribute. You should use this widget when you want to limit the values that can be written to an attribute in run mode. 

Widget setting 
===============

The user has the ability to:

- Add title to the submit button
- Add title to dropdown
- Add write values
- Show device name (boolean)
- Choose attribute display type (Label, Name or None)
- Set text color (string)
- Set background color (string)
- Set text size (number)
- Choose font type (Helvetica or Monospaced )
- Apply CSS to dropdown title
- Apply CSS to submit button

\ |IMG1|\ 

Widget design 
===============

\ |IMG2|\ 

*Notice: The widget accepts only input of type string, number and boolean.*

Good to know about this widget:

- If attribute has different type than the supported types the "{attribute} Not implemented" shows up.
- If the write value has wrong type the "Invalid type for value" message will show up on the option for that value and the option is disabled.
- For attribute of type boolean the write values has to be "true" or "false", other value than that is classified as invalid type.
- The label of a write value is not required. When the label is empty the write value will show up instead.
- Hovering over an option to see write value (In case the write value has label).

.. bottom of content

.. |IMG1| image:: _static/img/attribute_writer_dropdown_1.png
   :height: 885 px
   :width: 292 px

.. |IMG2| image:: _static/img/attribute_writer_dropdown_2.png
   :height: 180 px
   :width: 460 px