History of changes to WebJive
*****************************

The current version is |version|.

Latest changes on |version|:

    - Improve validation on Attribute Writer Widget
    - Improve UI of Attribute Writer widget and allow user to add custom css
    - Webjive is able to upload a dashboard even when a widget has been removed from the available ones


Previous versions
=====================

* version 1.1.4:

    - Fix the maximum height of canvas. user should be able to select, drop, delete, drag widget in inactive(white) area

* version 1.1.3:

    - Multiple swim lanes in timeline widget fixed issue with pause data update
    - Improve adding new device in all widgets using complex input(+) ex. Timeline, Attribute Plot, Spectrum2D etc

* version 1.1.2:

    - Notify user about logout on expiry login token
    - Added documentation related to old widgets
    - Draggable modal bug fixed. Now the users can drag all the modal dialogs
    - Improved notification area. Now the users can open the old notifications without closing the unread ones

* version 1.1.1: 

    - Fixed device selection issue on ``Device Status`` widget, also added documentation for widget


* version 1.1.0:

    - There is a mechanism for configuring a dashboard and specifying a Tango device class for the variable, as well a variable name and a default device instance for the variable
    - The device inspector allows specification of the variable name and selection of attributes of its instances
    - There is a new widget that can be used in a running dashboard to display the variable name and a dropdown menu to select an instance of the device class associated to the variable
    - All widgets are extended to cope with dashboard variables
    - Import / export dashboard variables along with dashboards
    - When importing a dashboard, Taranta checks that the device class and default device exist, and if not warns the user and provides whatever info is needed and possible for the user to amend the dashboard
    - Run time functionality for variable selector widget
    - Fix tabs navigation on taranta

* version 1.0.9:

* version 1.0.8:

    - Add timeline widget
    - Add devState to timeline widget
    - Fix dashboard load warnings
    - Fix notification load warnings
    - Fix input field concatenation warnings


* version 1.0.7:

    - Fix boolean command on devices
    - Add hover datatype display on devices
    - Fix command output display on devices
    - Add JSON command output display on devices
    - Fix output dashboard widget to unique widget display
    - Add command array widget max output display size
    - Add Modal output display to command array widget
    - Add JSON display to command array widget
    - Add timestamp build on version hover
    - Remove cooldown from command array widget
    - Add shared input control to command array widget
    - Add string type on attribute write
    - Add common input validation on attribute write
    - Add DevEnum attribute write capabilities
    - Add configuration variables to customize Webjive installation
    - Add the ability to upload a file as an argument of a DevString Command
    - Fix Led Widget to size properly the led at minimum when the MIN_WIDGET_SIZE value is 10

* version 1.0.6:

    - Added JSON display on dashboard attribute display widget
    - Added type of attribute on dashboards attribute selection
    - Added state persistency on navigation between dashboard and devices
    - Added draggable modals pop-ups
    - Added Multi-tenant charts to deploy Skampi 
    - Added Spectrum 2D widget
    - Added Embed page widget
    - Added Four channel scope widget
    - Refactor notification area 

* version 1.0.5:

    - Added new notification area
    - Fix void bug on switch widget
    - Added None display on attribute boolean display widget
    - Improved AttributeDisplay visualization
    - Improved SpectrumTable visualization
    - Refactor Documentation
    - Improve testing on widgets
    - Added Drag and drop on dashboard layer 

* version 1.0.4:

    - Added commandFile widget
    - Added k8s charts 
    - Fixed topBar display on smaller windows
    - Add Umbrella charts to WebJive
    - Updated react-boostrap
    - Improve testing on widgets

* WIP
