Spectrum-2D Widget
*********************

A widget that allows plotting multiple spectrum attributes on the y-axis against a single spectrum attribute on the x-axis. 

Widget setting 
==============
The widget allows for multiple spectrum attributes to be specified in a one-to-many configuration; one for the x-axis and many for the y-axis, respectively.
The ``Time Base`` is a spectrum attribute for the x-axis of the oscilloscope. Since the widget supports multiple y-axis spectrum attributes to be added, pressing the "+" sign next to ``Graphs`` will enable the user to specify a spectrum attribute.
Other settings include the option of displaying the ``Label`` or ``Name`` of the attributes being plotted and a checkbox for whether or not the title of the plot should be visible. This ``Show Title`` option affects the padding of the plot.

\ |IMG1|\ 

*The y-axis contains four spectrum attributes representing four dictinct channels and their waveform voltages from an ossiloscope plotted against another spectrum attribute called time-range*


Widget design
==============
The plot is rendered using plotly, a robust and interactive graphing library. The plots are re-drawn as they are updated by spectrum attribute data pushed by the respective tango device that the attributes associate with. PLotly, natively provides a couple of tools to interact with the plots. These include, zooming, panning, focusing on specific graph parts, resetting axes and saving snapshots of the graph as Portable Network Graphics(PNG) files. 


\ |IMG2|\ 

*The plot shown above is a sample of a four channel oscilloscope plotted against (psuedo)time-range*

.. bottom of content

.. |IMG1| image:: _static/img/spectrum_2d_settings.png

.. |IMG2| image:: _static/img/spectrum_2d_design.png