Boolean Display Widget
***********************

The **Boolean Display Widget** enables the user to set boolean values by using a switch button.

Widget setting 
===============

Widget is simple, the user has the ability to select device, attribute and one additional setting:

* Show device name (boolean)

\ |IMG1|\ 

Widget design 
===============

\ |IMG2|\ 

.. bottom of content

.. |IMG1| image:: _static/img/boolean_display_widget_settings.png
   :height: 159 px
   :width: 286 px

.. |IMG2| image:: _static/img/boolean_display_widget.png
   :height: 39 px
   :width: 279 px