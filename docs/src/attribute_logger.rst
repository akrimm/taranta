Attribute Logger
****************

The widget shows a value coming from an attribute in a log format using the timestamp. 

Widget setting 
===============

The user has the ability to:

- Attribute to log: select the device and the attribute to log
- Lines Logged: set the number of lines logged 
- Show device: if selected, show the device name
- Attribute Display: select if show the name or the label of the device. Label is shown as default
- Log if changed: if selected, the widget add the new log line only when a new value is changed. Otherwise it chagee the value every 2s.

\ |IMG1|\ 

Widget design 
===============

\ |IMG2|\ 



.. bottom of content

.. |IMG1| image:: _static/img/attribute_logger_inspector.png


.. |IMG2| image:: _static/img/attribute_logger_view.png
