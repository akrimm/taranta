KUBE_NAMESPACE?=testnamespace
CI_COMMIT_SHA?=local
VERSION?=$(shell cat package.json | grep version | head -1 | awk -F: '{ print $$2 }' | sed 's/[\",]//g' | tr -d '[[:space:]]')
HELM_RELEASE?=webjive1

# Fixed variables
TIMEOUT = 86400

# Docker and Gitlab CI variables
RDEBUG ?= ""
CI_ENVIRONMENT_SLUG ?= development
CI_PIPELINE_ID ?= pipeline$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=8 count=1 2>/dev/null;echo)
CI_JOB_ID ?= job$(shell tr -c -d '0123456789abcdefghijklmnopqrstuvwxyz' </dev/urandom | dd bs=4 count=1 2>/dev/null;echo)
GITLAB_USER ?= ""
CI_BUILD_TOKEN ?= ""
REPOSITORY_TOKEN ?= ""
REGISTRY_TOKEN ?= ""
GITLAB_USER_EMAIL ?= "nobody@example.com"
DOCKER_VOLUMES ?= /var/run/docker.sock:/var/run/docker.sock
CI_APPLICATION_TAG ?= $(shell git rev-parse --verify --short=8 HEAD)
DOCKERFILE ?= Dockerfile
EXECUTOR ?= docker
STAGE ?= build_react_artefacts

# define overides for above variables in here
-include PrivateRules.mak

watch:
	@watch kubectl get pod -n $(KUBE_NAMESPACE);

namespace:
	@kubectl create namespace $(KUBE_NAMESPACE) || kubectl describe namespace $(KUBE_NAMESPACE)

install_webjive: namespace
	@helm dependency update charts/webjive-umbrella/; \
	helm install $(HELM_RELEASE) -n $(KUBE_NAMESPACE) charts/webjive-umbrella/

install_auth: namespace
	@helm dependency update charts/auth/; \
	helm install auth1 -n $(KUBE_NAMESPACE) charts/auth/

install_dashboards: namespace
	@helm dependency update charts/dashboards/; \
	helm install dashboards1 -n $(KUBE_NAMESPACE) charts/dashboards/

template:
	@helm dependency update charts/webjive-umbrella/; \
	helm template $(HELM_RELEASE) -n $(KUBE_NAMESPACE) charts/webjive/

uninstall_all: uninstall_auth uninstall_dashboards uninstall_webjive

uninstall_webjive:
	@helm uninstall $(HELM_RELEASE) -n $(KUBE_NAMESPACE); \
	kubectl -n $(KUBE_NAMESPACE) wait --all --for=delete pods --timeout=300s

uninstall_auth:
	@helm uninstall auth1 -n $(KUBE_NAMESPACE); 

uninstall_dashboards:
	@helm uninstall dashboards1 -n $(KUBE_NAMESPACE); 

reinstall_all: uninstall_auth uninstall_dashboards uninstall_webjive install_auth install_dashboards install_webjive

reinstall_webjive: uninstall_webjive install_webjive

reinstall_auth: uninstall_auth install_auth

reinstall_dashboards: uninstall_dashboards install_dashboards

install_all: install_auth install_dashboards install_webjive

build-webjive:
	@docker build . -t artefact.skao.int/ska-webjive:latest --label GIT_COMMIT=$(CI_COMMIT_SHA)
	docker tag artefact.skao.int/ska-webjive:latest artefact.skao.int/ska-webjive:$(VERSION)

build-webjive-node:
	@docker build . -f Dockerfile.node -t artefact.skao.int/ska-webjive-node:latest --label GIT_COMMIT=$(CI_COMMIT_SHA)
	docker tag artefact.skao.int/ska-webjive-node:latest artefact.skao.int/ska-webjive-node:$(VERSION)

rbuild-react:  ## run make $(STAGE) using gitlab-runner
	gitlab-runner --log-level debug exec $(EXECUTOR) \
	--docker-volumes  $(DOCKER_VOLUMES) \
	--docker-privileged \
	--docker-pull-policy always \
	--timeout $(TIMEOUT) \
		--env "GITLAB_USER=$(GITLAB_USER)" \
		--env "REGISTRY_TOKEN=$(REGISTRY_TOKEN)" \
		--env "CI_BUILD_TOKEN=$(CI_BUILD_TOKEN)" \
		--env "KUBE_NAMESPACE=ci-$(CI_COMMIT_SHA)" \
		--env "TRACE=1" \
		--env "DEBUG=1" \
	$(STAGE) 2>&1 | tee log.txt || true
