import React from "react";
import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import { Provider } from "react-redux";
import { MemoryRouter, Route, Switch } from "react-router";

import store from "../../../dashboard/state/store";
import { NavContainer, Navbar } from "./Navbar";

configure({ adapter: new Adapter() });

let loadStateUrls = jest.fn();
let saveStateUrls = jest.fn();

describe("Test Navbar", () => {
  it("renders without crashing", () => {

    let sampleStateUrl = {
      devices: '/testdb/devices',
      dashboard: '/testdb/dashboard'
    }

    const element = React.createElement(Navbar.WrappedComponent, {
        stateUrls: sampleStateUrl,
        onLoadStateUrls: loadStateUrls,
        onSaveStateUrls: saveStateUrls
    })

    const content = mount(<Provider store={store}>{element}</Provider>);

    content.setProps({
      username: "CREAM",
      stateUrls: sampleStateUrl,
      onLoadStateUrls: jest.fn(),
      onSaveStateUrls: jest.fn()
    });

    expect(content.find('.navigation').length).toEqual(1);
  });

  it('test Navbar with Memory Router for Navbar & NavContainer', async () => {
    const wrapper = mount(
      <MemoryRouter initialEntries={['/testdb/devices']}>
        <Provider store={store} >
          <Switch>
            <Route path="/testdb/devices" render={() => <Navbar />} />
          </Switch>
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper.find(Navbar)).toHaveLength(1);

    let params = {
      section: "dashboard",
      tangoDB: "testdb"
    }
    let stateUrls = {
      deviceUrl: '',
      dashboardUrl: ''
    }
    const wrapper1 = mount(
      <MemoryRouter initialEntries={['/testdb/devices']}>
        <Provider store={store} >
          <Switch>
            <Route path="/testdb/devices" render={() => <NavContainer params={params} saveStateUrls={saveStateUrls} stateUrls={stateUrls} />} />
          </Switch>
        </Provider>
      </MemoryRouter>
    );

    expect(wrapper1.find(NavContainer)).toHaveLength(1);
    const NavHtml = wrapper1.find(NavContainer).html();
    expect(NavHtml).toContain('non-active');
    expect(NavHtml).toContain('active');
    expect(NavHtml).toContain('pagelinks');
    expect(NavHtml).toContain('tabbed-menu');
    expect(NavHtml).toContain('Devices');
    expect(NavHtml).toContain('Dashboards');

    wrapper1.find(NavContainer).find('.tabbed-menu.active').simulate('click');
    expect(saveStateUrls).toHaveBeenCalledTimes(1);

    wrapper1.find(NavContainer).find('.non-active').simulate('click');
    expect(saveStateUrls).toHaveBeenCalledTimes(2);
  });
});
