import {
  SAVE_STATE_URL,
  SAVE_STATE_URL_SUCCESS,
  SAVE_STATE_URL_FAILED,
  LOAD_STATE_URL,
  LOAD_STATE_URL_SUCCESS,
  LOAD_STATE_URL_FAILED,
  LOGIN,
  LOGIN_SUCCESS,
  LOGOUT,
  LOGIN_FAILED,
  LOGOUT_SUCCESS,
  PRELOAD_USER_SUCCESS,
  PRELOAD_USER_FAILED,
  PRELOAD_USER,
  EXTEND_LOGIN,
  EXTEND_LOGIN_SUCCESS,
  EXTEND_LOGIN_FAILED,
  OPEN_LOGIN_DIALOG,
  CLOSE_LOGIN_DIALOG,
  SAVE_NOTIFICATIONS,
  SAVE_NOTIFICATIONS_FAILED,
  SAVE_NOTIFICATIONS_SUCCESS,
  LOAD_NOTIFICATIONS,
  LOAD_NOTIFICATIONS_FAILED,
  LOAD_NOTIFICATIONS_SUCCESS,
  CLEAR_NOTIFICATIONS,
  CLEAR_NOTIFICATIONS_FAILED,
  CLEAR_NOTIFICATIONS_SUCCESS,
  SET_NOTIFICATIONS,
  SET_NOTIFICATIONS_FAILED,
  SET_NOTIFICATIONS_SUCCESS,
} from "./actionTypes";

import { Action } from "redux";

import { Notification } from "../../notifications/notifications";

import { StateUrls } from "../../ui/navbar/Navbar";

export interface ISaveStateUrlAction extends Action {
  type: typeof SAVE_STATE_URL;
  stateUrls: StateUrls;
}

export interface ISaveStateUrlSuccessAction extends Action {
  type: typeof SAVE_STATE_URL_SUCCESS;
  stateUrls: StateUrls;
}

export interface ISaveStateUrlFailedAction extends Action {
  type: typeof SAVE_STATE_URL_FAILED;
}

export interface ILoadStateUrlAction extends Action {
  type: typeof LOAD_STATE_URL;
}

export interface ILoadStateUrlSuccessAction extends Action {
  type: typeof LOAD_STATE_URL_SUCCESS;
  stateUrls: StateUrls;
}

export interface ILoadStateUrlFailedAction extends Action {
  type: typeof LOAD_STATE_URL_FAILED;
}

export interface ILoginAction extends Action {
  type: typeof LOGIN;
  username: string;
  password: string;
}

export interface ILoginSuccessAction extends Action {
  type: typeof LOGIN_SUCCESS;
  username: string;
  userGroups: string[];
}

export interface ILogoutAction extends Action {
  type: typeof LOGOUT;
}

export interface ILogoutSuccessAction extends Action {
  type: typeof LOGOUT_SUCCESS;
}

export interface ILoginFailedAction extends Action {
  type: typeof LOGIN_FAILED;
}

export interface IPreloadUserAction extends Action {
  type: typeof PRELOAD_USER;
}

export interface IPreloadUserSuccessAction extends Action {
  type: typeof PRELOAD_USER_SUCCESS;
  username: string;
  userGroups: string[];
}

export interface IPreloadUserFailedAction extends Action {
  type: typeof PRELOAD_USER_FAILED;
}

export interface IExtendLoginAction extends Action {
  type: typeof EXTEND_LOGIN;
}

export interface IExtendLoginSuccessAction extends Action {
  type: typeof EXTEND_LOGIN_SUCCESS;
}

export interface IExtendLoginFailedAction extends Action {
  type: typeof EXTEND_LOGIN_FAILED;
}

export interface IOpenLoginDialogAction extends Action {
  type: typeof OPEN_LOGIN_DIALOG;
}

export interface ICloseLoginDialogAction extends Action {
  type: typeof CLOSE_LOGIN_DIALOG;
}

export interface ISaveNotificationAction extends Action {
  type: typeof SAVE_NOTIFICATIONS;
  notification: Notification;
  username: string;
}

export interface ISaveNotificationSuccessAction extends Action {
  type: typeof SAVE_NOTIFICATIONS_SUCCESS;
  notifications: Notification[];
}

export interface ISaveNotificationFailedAction extends Action {
  type: typeof SAVE_NOTIFICATIONS_FAILED;
}

export interface ILoadNotificationAction extends Action {
  type: typeof LOAD_NOTIFICATIONS;
  username: string;
}

export interface ILoadNotificationSuccessAction extends Action {
  type: typeof LOAD_NOTIFICATIONS_SUCCESS;
  notifications: Notification[];
}

export interface ILoadNotificationFailedAction extends Action {
  type: typeof LOAD_NOTIFICATIONS_FAILED;
}

export interface IClearNotificationAction extends Action {
  type: typeof CLEAR_NOTIFICATIONS;
  username: string;
}

export interface IClearNotificationSuccessAction extends Action {
  type: typeof CLEAR_NOTIFICATIONS_SUCCESS;
  notifications: Notification[];
}

export interface IClearNotificationFailedAction extends Action {
  type: typeof CLEAR_NOTIFICATIONS_FAILED;
}

export interface ISetNotificationAction extends Action {
  type: typeof SET_NOTIFICATIONS;
  key: number;
}

export interface ISetNotificationSuccessAction extends Action {
  type: typeof SET_NOTIFICATIONS_SUCCESS;
  notifications: Notification[];
}

export interface ISetNotificationFailedAction extends Action {
  type: typeof SET_NOTIFICATIONS_FAILED;
}
