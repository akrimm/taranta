import React from "react";
import ReactJSONTree from "react-json-tree";

const theme = {
  base00: "#000000",
  base09: "#005cc5", // numbers
  base0B: "#8e24aa", // strings
  base0D: "#565656" // keys
};

export const JSONTree = ({ data }) => {
  const hideRoot = Object.keys(data).length <= 5;
  return (
    <ReactJSONTree
      data={data}
      theme={theme}
      getItemString={() => null}
      hideRoot={hideRoot}
      shouldExpandNode={() => false}
      labelRenderer={keyPath => {
        const first = keyPath[0];
        return keyPath.length === 1 && first === "root" ? (
          <span style={{ fontStyle: "italic" }}>Object</span>
        ) : (
          first
        );
      }}
    />
  );
};

export function parseJSONObject(str) {
  try {
    const obj = JSON.parse(str);
    return typeof obj === "object" ? obj : null;
  } catch (err) {
    return null;
  }
}  
