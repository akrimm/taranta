import collapseAttribute from './CollapseAttribute'

describe("test util Collapse Attribute function", () =>{

    const attribute1 = {attribute: {
        attribute: "attributeName1", 
        enumlabels: ["Empty", "Idle", "Ready"]
        } };

    const attribute2 = {attribute: {
        attribute: "attributeName1", 
        enumlabels: ["Empty", "Idle", "Ready"]
        } };

    const attribute3 = {attribute: {
        attribute: "attributeName3", 
        enumlabels: ["Ready", "Scan"]
        } };



    it("test if it returns the same object it the comparison argument is different from enumlabels or attributename ", ()=>{
        expect(collapseAttribute([attribute1, attribute2], "value")).toEqual([attribute1, attribute2]);
    })

    it("test if it collapses the array using enumlabels", () =>{

        expect(collapseAttribute([attribute1, attribute2, attribute3], "enumlabels")).toEqual([[attribute1, attribute2],[attribute3]]);

    })

    it("test if it collapses the array using attributename", () =>{

        expect(collapseAttribute([attribute1, attribute2, attribute3], "attributename")).toEqual([[attribute1, attribute2],[attribute3]]);

    })
})