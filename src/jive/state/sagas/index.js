import { fork } from "redux-saga/effects";

import createUserSaga from "../../../shared/user/state/saga";
import tango from "./tango";
import mongodb from "./mongodb";

export default function* sagas() {
  yield fork(createUserSaga());
  yield fork(tango);
  yield fork(mongodb);
}
