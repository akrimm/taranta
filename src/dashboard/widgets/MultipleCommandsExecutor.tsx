import React, { Component, CSSProperties } from "react";

import { WidgetProps } from "./types";
import {
  WidgetDefinition,
  StringInputDefinition,
  BooleanInputDefinition,
  CommandInputDefinitionMultiple,
  NumberInputDefinition,
  ColorInputDefinition,
  SelectInputDefinition
} from "../types";

function timeout(seconds) {
  return new Promise(resolve => {
    setTimeout(() => resolve(), seconds);
  });
}

type Inputs = {
  title: StringInputDefinition;
  requireConfirmation: BooleanInputDefinition;
  command: CommandInputDefinitionMultiple;
  displayOutput: BooleanInputDefinition;
  cooldown: NumberInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  cooldown: boolean;
}

class MultipleCommandsExecutor extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { cooldown: false };
    this.handleExecute = this.handleExecute.bind(this);
  }

  public render() {
    const { inputs } = this.props;
    const { title, command, backgroundColor, textColor, displayOutput } = inputs;

    const { containerStyle, fullOutputStyle } = this.getStyles();

    let actualTitle = title || "commands";

    let commands : any[] = [];

    if (command && command.command && command.command.length === command.output.length) {
      commands = command.command.map((cmd, index) => ({
        'command': cmd,
        'output': displayOutput ? command.output[index] || 'n/a' : null
      }));
    }

    return (
      <div style={containerStyle}>
        <div style={{
            border:"none",
            borderRadius: "2px",
            color: backgroundColor,
            backgroundColor: textColor,
            width: '150px',
            padding: '0 0 0 5px',
            margin: '0 0 10px 0',
          }}
        >
          {actualTitle}
        </div>
        {
          commands.map((el) => (
            <div key={el.command}>
              <button
                style={{
                  border: "none",
                  borderRadius: "2px",
                  color: backgroundColor,
                  backgroundColor: textColor,
                  marginBottom: "10px",
                }}
                disabled={this.state.cooldown}
                onClick={() => this.handleExecute(el.command)}
                >
                  {el.command}
              </button>
              <span style={fullOutputStyle}>{el.output || 'n/a'}</span>
            </div>
          ))
        }
      </div>
    );
  }

  private async handleExecute(cmd) {
    const { command, requireConfirmation, cooldown } = this.props.inputs;

    const message = `Confirm executing "${cmd}" on "${command.device}"`;

    /* eslint-disable no-restricted-globals */
    if (!requireConfirmation || confirm(message)) {
      command.execute(null, cmd);

      if (cooldown > 0) {
        this.setState({ cooldown: true });
        await timeout(1000 * cooldown);
        this.setState({ cooldown: false });
      }
    }
  }

  private getStyles() {
    const { mode, inputs } = this.props;
    const { size, font, backgroundColor, textColor } = inputs;
  
    const extraStyle = mode === "library" ? { margin: "0.25em" } : {};
    const containerStyle: CSSProperties = {
      padding: "0.25em",
      backgroundColor,
      color: textColor,
      fontSize: size + "em",
      ...extraStyle
    };
    if (font){
      containerStyle["fontFamily"] = font;
    }

    const [, outputStyle] = this.outputAndStyle();
    const fullOutputStyle: CSSProperties = {
      marginLeft: "0.5em",
      ...outputStyle
    };

    return { containerStyle, fullOutputStyle }
  }

  private outputAndStyle(): [string, CSSProperties] {
    const { mode, inputs } = this.props;
    const { command, displayOutput } = inputs;
    const { output } = command;
    const shownOutput = displayOutput
      ? mode === "run"
        ? output
        : "output"
      : "";

    if (mode !== "run") {
      return [shownOutput, { color: "gray", fontStyle: "italic" }];
    }

    if (displayOutput && output === undefined) {
      return ["n/a", { color: "gray" }];
    }

    return [shownOutput, {}];
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "MULTIPLE_COMMANDS_EXECUTOR",
  name: "Multiple Commands Executor",
  defaultHeight: 20,
  defaultWidth: 15,
  inputs: {
    command: {
      label: "",
      type: "command",
      required: true,
      intype: "DevVoid"
    },
    title: {
      type: "string",
      label: "Title",
      default: ""
    },
    requireConfirmation: {
      type: "boolean",
      label: "Require Confirmation",
      default: true
    },
    displayOutput: {
      type: "boolean",
      label: "Display Output",
      default: true
    },
    cooldown: {
      type: "number",
      label: "Cooldown (s)",
      default: 0,
      nonNegative: true
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000"
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff"
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica"
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new"
        }
      ]
    }
  }
};

export default { definition, component: MultipleCommandsExecutor };
