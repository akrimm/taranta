import React, { Component, CSSProperties } from "react";
import { WidgetProps } from "./types";

import {
  WidgetDefinition,
  NumberInputDefinition,
  BooleanInputDefinition,
  CommandInputDefinition,
  StringInputDefinition,
  ColorInputDefinition,
  SelectInputDefinition,
} from "../types";
import { Button } from "react-bootstrap";

import Modal from "../../shared/modal/components/Modal/Modal";

import OutputDisplay from "../../shared/utils/OutputDisplay";

import InputField from "../../shared/utils/InputField";

type Inputs = {
  command: CommandInputDefinition;
  showDevice: BooleanInputDefinition;
  showCommand: BooleanInputDefinition;
  title: StringInputDefinition;
  buttonText: StringInputDefinition;
  requireConfirmation: BooleanInputDefinition;
  displayOutput: BooleanInputDefinition;
  OutputMaxSize: NumberInputDefinition;
  timeDisplayOutput: NumberInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
  input: string;
  pending: boolean;
  showModal: boolean;
}

let waiting = false,
  displayOutputCommand = 0;

class CommandArray extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = {
      input: "",
      pending: false,
      showModal: false,
    };
    this.handleExecute = this.handleExecute.bind(this);
  }

  public render() {
    const { inputs, id } = this.props;
    const {
      command,
      showDevice,
      showCommand,
      backgroundColor,
      textColor,
      size,
      font,
      timeDisplayOutput,
      OutputMaxSize,
    } = inputs;

    const { device, command: commandName } = command;
    const acceptedType: string = command["acceptedType"]
      ? command["acceptedType"]
      : "DevString";

    const deviceLabel = device || "device";
    const commandLabel = commandName || "command";

    const label = [
      ...(showDevice ? [deviceLabel] : []),
      ...(showCommand ? [commandLabel] : []),
    ].join("/");

    let output = command.output;

    if (
      output[0] !== undefined &&
      displayOutputCommand === id &&
      !waiting &&
      timeDisplayOutput !== 0
    ) {
      waiting = true;
      setTimeout(() => {
        displayOutputCommand = 0;
        waiting = false;
        this.setState({});
      }, timeDisplayOutput);
    }

    const style: CSSProperties = {
      display: "flex",
      alignItems: "center",
      padding: "0.25em 0.5em",
      backgroundColor,
      color: textColor,
      fontSize: size + "em",
    };
    if (font) {
      style["fontFamily"] = font;
    }

    let buttonLabel = this.props.inputs.buttonText || "Submit";
    let titleLabel = this.props.inputs.title;
    let outputSpan = <div />;

    const outputAll = output;
    let viewMore = <div />;
    if (
      output.toString().length > OutputMaxSize &&
      displayOutputCommand === id
    ) {
      output = output.toString().substr(0, OutputMaxSize) + "...";
      viewMore = (
        <div
          style={{
            fontSize: "10px",
            float: "left",
            cursor: "pointer",
            textDecoration: "underline",
            color: "blue",
          }}
          id="viewmore"
          onClick={() => this.openModal()}
        >
          View More
        </div>
      );
    }

    if (displayOutputCommand === id) {
      outputSpan = (
        <div
          style={{
            maxHeight: this.props.actualHeight + "px",
            fontSize: "11px",
            paddingLeft: "3px",
          }}
          title={outputAll}
        >
          <OutputDisplay value={output} isLoading={false} />
        </div>
      );
    }

    let modal = (
      <Modal title={device + "/" + commandName} transparentModal={true}>
        <Modal.Body>
          <OutputDisplay value={outputAll} isLoading={false} />
        </Modal.Body>
        <Modal.Footer>
          <Button
            id="btn-close"
            variant="primary"
            onClick={() => this.closeModal()}
          >
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    );

    return (
      <div style={style}>
        <table className="separated">
          <tbody>
            <tr>
              <td>
                {titleLabel && (
                  <span style={{ flexGrow: 0, marginRight: "0.5em" }}>
                    {titleLabel}
                  </span>
                )}
                {label && (
                  <span style={{ flexGrow: 0, marginRight: "0.5em" }}>
                    {label}:
                  </span>
                )}
              </td>
              <td>
                <InputField
                  isEnabled={true}
                  onExecute={this.handleExecute}
                  intype={acceptedType}
                  buttonLabel={buttonLabel}
                />
              </td>
              <td>
                {outputSpan}
                {viewMore}
              </td>
              {this.state.showModal && modal}
            </tr>
          </tbody>
        </table>
      </div>
    );
  }

  private async handleExecute(commandv, value) {
    if (this.state.pending) {
      return;
    }

    const { command, requireConfirmation } = this.props.inputs;

    let acceptedType = this.props.inputs.command["dataType"];
    if (!acceptedType) acceptedType = this.props.inputs.command["acceptedType"];

    const parameterMessage = ` with parameter ` + value;
    let message = `Confirm executing ${command.command} on ${command.device}`;
    if (acceptedType)
      message += acceptedType.includes("Void") ? "" : parameterMessage;

    /* eslint-disable no-restricted-globals */
    if (!requireConfirmation || confirm(message)) {
      this.setState({ input: "", pending: true });

      displayOutputCommand = this.props.id;
      command.execute(value);

      this.setState({ input: "", pending: false });
    }
  }

  openModal() {
    this.setState({ showModal: true });
  }

  closeModal() {
    this.setState({ showModal: false });
  }
} // class CommandArray

const definition: WidgetDefinition<Inputs> = {
  type: "COMMAND_ARRAY",
  name: " Command Array",
  defaultHeight: 3,
  defaultWidth: 20,
  inputs: {
    title: {
      type: "string",
      label: "Title",
    },
    buttonText: {
      type: "string",
      label: "Button Label",
    },
    command: {
      label: "",
      type: "command",
      required: true,
      intype: "Any",
    },
    showDevice: {
      type: "boolean",
      label: "Show Device Name",
      default: true,
    },
    showCommand: {
      type: "boolean",
      label: "Show Commnad Name",
      default: true,
    },
    requireConfirmation: {
      type: "boolean",
      label: "Require Confirmation",
      default: true,
    },
    displayOutput: {
      type: "boolean",
      label: "Display Output",
      default: true,
    },
    OutputMaxSize: {
      type: "number",
      label: "Maximum size for output display",
      default: 20,
      nonNegative: true,
    },
    timeDisplayOutput: {
      type: "number",
      label: "Time out for output display ms",
      default: 3000,
      nonNegative: true,
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000",
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff",
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 1,
      nonNegative: true,
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica",
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new",
        },
      ],
    },
  },
};

export default {
  definition,
  component: CommandArray,
};
