import React from "react";
import { configure, shallow, mount } from "enzyme";
import { Provider } from "react-redux";
import Adapter from "enzyme-adapter-react-16";
import store from "../../dashboard/state/store";
import Parametric, { PW } from "./Parametric";

configure({ adapter: new Adapter() });
describe("Parametric Widget", () => {

  let changeDevice = jest.fn();
  const selectedDashboard = {"selectedId":null,"selectedIds":["1"],"widgets":{"1":{"_id":"60910a16464196001183c1c4","id":"1","x":16,"y":3,"canvas":"0","width":13,"height":4,"type":"PARAMETRIC_WIDGET","inputs":{"name":"MName","variable":"myvar"},"order":0,"valid":true}},"id":"604b5fac8755940011efeea1","name":"Untitled dashboard","user":"user1","group":null,"groupWriteAccess":false,"lastUpdatedBy":"user1","insertTime":"2021-03-12T12:33:48.981Z","updateTime":"2021-05-05T15:07:22.173Z","history":{"undoActions":[],"redoActions":[],"undoIndex":0,"redoIndex":0,"undoLength":0,"redoLength":0},"variables":[]}
  const dashboards = [{"id":"604b5fac8755940011efeea1","name":"Untitled dashboard","user":"user1","insertTime":"2021-03-12T12:33:48.981Z","updateTime":"2021-05-05T15:07:22.173Z","group":null,"lastUpdatedBy":"user1","tangoDB":"testdb","variables":[{"id":"0cef8i6340ij7","name":"myvar","class":"DServer","device":"dserver/DataBaseds/2"}]},{"id":"6073154703c08b0018111066","name":"ajay","user":"user1","insertTime":"2021-04-11T15:27:03.803Z","updateTime":"2021-05-04T13:01:40.904Z","group":null,"lastUpdatedBy":"user1","tangoDB":"testdb"}];
  const tangoClasses = [
    {"name":"DataBase","devices":[{"name":"sys/database/2"}]},
    {"name":"DServer","devices":[{"name":"dserver/DataBaseds/2"},{"name":"dserver/Starter/f5d56d5f249f"},{"name":"dserver/TangoAccessControl/1"},{"name":"dserver/TangoTest/test"},{"name":"dserver/WebjiveTestDevice/test"}]},
    {"name":"Starter","devices":[{"name":"tango/admin/f5d56d5f249f"}]},{"name":"TangoAccessControl","devices":[{"name":"sys/access_control/1"}]},{"name":"TangoTest","devices":[{"name":"sys/tg_test/1"}]},{"name":"WebjiveTestDevice","devices":[{"name":"test/webjivetestdevice/1"}]}];

  it("renders without crashing", () => {
    const element = React.createElement(Parametric.component, {
      mode: "run",
      t0: 1,
      // id: 22,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        name: "Variable1",
        variable: "myvar"
      },
      tangoDB: 'testdb',
      tangoClasses: tangoClasses
    });

    const content = mount(<Provider store={store}>{element}</Provider>);

    content.setProps({
      username: "CREAM",
      selectedDashboard: selectedDashboard,
      dashboards: dashboards,
      changeDevice: changeDevice(),
      fetchClasses: (() => {return tangoClasses})
    });

    expect(content.html()).toContain('Variable1');
    expect(content.find('.parametric-dropdown').length).toEqual(1);
    expect(content.find('.parametric-dropdown').html()).toContain('No device found');
    expect(content.find('.parametric-dropdown').html()).not.toContain('sys/tg_test/1');
    content.find('.parametric-dropdown').simulate('change');
  });

  it("renders without name variable", () => {
    const fetchClasses = jest.fn();
    const saveDashboard = jest.fn();
    const elementHtml = React.createElement(PW.WrappedComponent, {
      mode: "run",
      t0: 1,
      // id: 22,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        variable: "myvar"
      },
      selectedDashboard: selectedDashboard,
      dashboards: dashboards,
      tangoDB: 'testdb',
      fetchClasses: fetchClasses,
      saveDashboard: saveDashboard
    });

    const shallowElement = shallow(elementHtml);
    // fetchClasses.mockReturnValueOnce(tangoClasses);
    shallowElement.setState({tangoClasses: tangoClasses});

    expect(shallowElement.html()).toContain('myvar');
    expect(shallowElement.html()).toContain('dserver/DataBaseds/2');
    expect(shallowElement.html()).toContain('dserver/Starter/f5d56d5f249f');

    shallowElement.find('.parametric-dropdown').simulate('change', { target: { value: "dserver/Starter/f5d56d5f249f" } });
    expect(saveDashboard).toHaveBeenCalledTimes(1);
    expect(shallowElement.find('.parametric-dropdown').getElement().props.value).toEqual('dserver/Starter/f5d56d5f249f');
  });
});