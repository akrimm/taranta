import React, { Component } from "react";

import Plot, { Trace } from "./Plot";
import { WidgetProps } from "../types";
import { Inputs } from ".";
import collapseAttribute from "../../../shared/utils/CollapseAttribute";

type Props = WidgetProps<Inputs>;
type AttributeEntries = Props["inputs"]["attributes"];

const tangoStates = ["ON","OFF","CLOSE","OPEN","INSERT","EXTRACT","MOVING",
"STANDBY","FAULT","INIT","RUNNING","ALARM","DISABLE","UNKNOWN"];

interface State {
  changeViewEnable: boolean;
  dataFlow: boolean;
  buttonLabel: string;
  viewLabel: string;
  eventView: boolean;
  dataHistory: [{}];
}

interface plotData {
  traces: Trace[];
  params: any;
}

class Timeline extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = {
      changeViewEnable: true,
      dataFlow: true,
      buttonLabel: "Pause Data update",
      viewLabel: "Show Event View",
      eventView: false,
      dataHistory: [{}],
    };
    this.changeDataFlow = this.changeDataFlow.bind(this);
    this.changeView = this.changeView.bind(this);
  }

  render() {
    const plotData: plotData = this.populateData();

    return (
      <div className="timeline-wrapper">
        <div className="options">
          <button style={{ borderRadius: "5px" }} onClick={this.changeDataFlow}>
            {this.state.buttonLabel}
          </button>
          <button
            style={{ borderRadius: "5px" }}
            disabled={!this.state.changeViewEnable}
            onClick={this.changeView}
          >
            {this.state.viewLabel}
          </button>
        </div>
        <Plot
          traces={plotData.traces}
          params={plotData.params}
          dataFlow={this.state.dataFlow}
          eventView={this.state.eventView}
        />
      </div>
    );
  }

  populateData() {
    const { mode, inputs, actualWidth, actualHeight } = this.props;
    const { attributes, timeWindow } = inputs;
    const runParams = {
      width: actualWidth,
      height: actualHeight,
      timeWindow,
      staticMode: false,
    };

    const staticParams = { ...runParams, staticMode: true };
    let plotData: plotData;

    if (mode === "run") {
      const traces = this.tracesFromAttributeInputs(attributes, this.props.t0);
      plotData = { traces: traces, params: runParams };
    } else if (mode === "library") {
      const xValues = Array(timeWindow)
        .fill(0)
        .map((_, i) => i);
      const sample1 = xValues.map(
        (x) => 8 * Math.sin(x / 6) * Math.sin(x / 20)
      );
      const sample2 = xValues.map(
        (x) => 5 * Math.cos(x / 20) * Math.cos(x / 3)
      );
      const traces: Trace[] = [
        {
          fullName: "attribute 1",
          x: xValues,
          y: sample1,
          axisLocation: "left",
          yAxisDisplay: "Label",
          position: 1,
        },
        {
          fullName: "attribute 2",
          x: xValues,
          y: sample2,
          axisLocation: "left",
          yAxisDisplay: "Label",
          position: 2,
        },
      ];

      plotData = { traces: traces, params: { ...staticParams, height: 150 } };
    } else {
      //edit
      const traces = attributes.map((attributeInput) => {
        const { device, attribute, label } = attributeInput.attribute;
        const { showAttribute } = attributeInput;

        let display = "";
        if (showAttribute === "Label") {
          if (label !== "") display = label;
          else display = "attributeLabel";
        } else if (showAttribute === "Name") {
          if (attribute !== null) display = attribute;
          else display = "attributeName";
        }

        const fullName = `${device || "?"}/${display || "?"}`;
        const trace: Trace = {
          fullName,
          yLabels: ["EnumLabels"],
          axisLocation: attributeInput.yAxis,
          yAxisDisplay: attributeInput.yAxisDisplay,
          position: 1
        };
        return trace;
      });

      plotData = {
        traces,
        params: staticParams,
      };
    }

    return plotData;
  }

  tracesFromAttributeInputs(
    complexInputs: AttributeEntries,
    t0: number
  ): Trace[] {
    let i = 1;
    return complexInputs.map((complexInput) => {
      const { attribute: attributeInput, yAxis } = complexInput;
      const { history, device, attribute } = attributeInput;
      const fullName = `${device}/${attribute}`;

      let x: number[] = [];
      let y: number[] = [];

      let yLabels = complexInput.attribute.enumlabels;

      if(complexInput.attribute.dataType === "DevState") yLabels = tangoStates;
      
      if (history.length > 0) {
        x = history.map(({ timestamp }) => timestamp - t0);
        if(complexInput.attribute.dataType === "DevState")
        y = history.map(({ value }) => tangoStates.indexOf(value));
        else y = history.map(({ value }) => value);
      }

      let position = i;
      if (this.props.inputs.groupAttributes) {
        const collapsedArray = collapseAttribute(complexInputs, "enumlabels");

        for (let _i = 0; _i < collapsedArray.length; _i++) {
          if (collapsedArray[_i].indexOf(complexInput) >= 0) {
            position = _i + 1;
          }
        }
      }
      i++;
      return {
        fullName,
        x,
        y,
        yLabels,
        axisLocation: yAxis,
        yAxisDisplay: complexInput.yAxisDisplay,
        position: position,
      };
    });
  }

  private changeDataFlow() {
    this.setState({
      dataFlow: !this.state.dataFlow,
      buttonLabel: this.state.dataFlow
        ? "Continue data update"
        : "Pause data update",
      changeViewEnable: this.state.dataFlow ? false : true,
    });
  }

  private changeView() {
    this.setState({ 
      eventView: !this.state.eventView,
      viewLabel: this.state.eventView
        ? "Show Event View"
        : "Show Time View"
    });
  }
}

export default Timeline;
