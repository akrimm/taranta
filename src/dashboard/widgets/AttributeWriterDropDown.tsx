import React, { Component, CSSProperties } from "react";
import { Button, Dropdown } from 'react-bootstrap';

import { WidgetProps } from "./types";
import {
    WidgetDefinition,
    AttributeInputDefinition,
    BooleanInputDefinition,
    ColorInputDefinition,
    NumberInputDefinition,
    SelectInputDefinition,
    AttributeInput,
    StringInputDefinition,
    ComplexInputDefinition,
    StyleInputDefinition
} from "../types";
import { parseCss } from "../components/Inspector/StyleSelector"

interface AttributeValueComplexInput {
    title: StringInputDefinition;
    value: StringInputDefinition;
}
type Inputs = {
    attribute: AttributeInputDefinition;
    dropdownTitle: StringInputDefinition;
    submitButtonTitle: StringInputDefinition;
    writeValues: ComplexInputDefinition<AttributeValueComplexInput>;
    showDevice: BooleanInputDefinition;
    showAttribute: SelectInputDefinition;
    textColor: ColorInputDefinition;
    backgroundColor: ColorInputDefinition;
    size: NumberInputDefinition;
    font: SelectInputDefinition;
    dropdownButtonCss: StyleInputDefinition;
    submitButtonCss: StyleInputDefinition;
};

type Props = WidgetProps<Inputs>;

interface State {
    pending: boolean;
    selectedValue: { title: string, value: string } | null;
}

class AttributeWriterDropdown extends Component<Props, State> {
    public constructor(props: Props) {
        super(props);
        this.state = {
            pending: false,
            selectedValue: null
        }
    }
    private dataType(): "numeric" | "boolean" | "string" | "other" {
        const { attribute } = this.props.inputs;
        const { dataType, isNumeric } = attribute;
        const isBoolean = dataType === "DevBoolean";
        const isString = dataType === "DevString";
        return isNumeric
            ? "numeric"
            : isBoolean
                ? "boolean"
                : isString
                    ? "string"
                    : "other";
    }

    private getDisplay(attribute: AttributeInput, showAttribute: string): string {
        let display = "";
        if (showAttribute === "Label") {
            if (attribute.label !== "") display = attribute.label;
            else display = "attributeLabel";
        }
        else if (showAttribute === "Name") {
            if (attribute.attribute !== null) display = attribute.attribute;
            else display = "attributeName";
        }
        return display;
    }

    private async handleSubmit() {
        const { attribute } = this.props.inputs;
        const { isNumeric } = attribute;
        const isBoolean = attribute.dataType === "DevBoolean";
        const selected = this.state.selectedValue;
        if (selected !== null) {
            const value = selected.value
            if (value === null || ((isNumeric || isBoolean) && (value === ""))) {
                return; //we don't want to interpret an emtpy string as a zero or false
            }
            let typeCastedValue: any = value;
            if (isNumeric) {
                typeCastedValue = Number(value);
            }
            if (isBoolean) {
                if (value.toLowerCase() === "false") {
                    typeCastedValue = false;
                } else if (value.toLowerCase() === "true") {
                    typeCastedValue = true;
                } else {
                    typeCastedValue = null;
                }
            }
            if (typeof value === "number" && isNaN(value)) {
                return;
            }
            if (typeof value === "boolean" && !typeCastedValue === null) {
                return;
            }
            this.setState({ pending: true });
            await this.props.inputs.attribute.write(typeCastedValue);
            this.setState({ pending: false });
        }
    }
    public render() {
        const { mode, inputs } = this.props;
        const {
            attribute,
            showDevice,
            dropdownTitle,
            submitButtonTitle,
            writeValues,
            showAttribute,
            backgroundColor,
            textColor,
            size,
            font,
            dropdownButtonCss,
            submitButtonCss
        } = inputs;
        const { selectedValue } = this.state;
        const submitButtonStyle = parseCss(submitButtonCss).data
        const dropdownButtonStyle = parseCss(dropdownButtonCss).data
        const { device} = attribute;
        const unit = mode === "run" ? attribute.unit : "unit";
        const deviceLabel = device || "device";
        let display = this.getDisplay(attribute, showAttribute);

        const displayWidget = [
            ...(showDevice ? [deviceLabel] : []),
            display
        ].join("/");

        const dataType = this.dataType();
        if (
            mode === "run" &&
            dataType !== "numeric" &&
            dataType !== "string" &&
            dataType !== "boolean"
        ) {
            return (
                <div style={{ backgroundColor: "red", padding: "0.5em" }}>
                    {attribute.dataType} not implemented
                </div>
            );
        }

        const isInvalid = (value) => {
            return ((dataType === "numeric" && isNaN(parseFloat(value))) ||
                (dataType === "boolean" && ["true", "false"].indexOf(value.toLowerCase()) === -1))
        }

        const style: CSSProperties = {
            display: "flex",
            alignItems: "center",
            padding: "0.25em 0.5em",
            backgroundColor,
            color: textColor,
            fontSize: size + "em"
        };
        if (font) {
            style["fontFamily"] = font;
        }
        const values = writeValues.length > 0 ? writeValues.map((input, idx) => {
            const invalid = isInvalid(input.value)

            return (
                <Dropdown.Item
                    key={idx}
                    disabled={invalid}
                    title={`write value: ${input.value} ${unit}`}
                    as="button"
                    onClick={() => this.setState({ selectedValue: input })}
                >
                    {invalid ?
                        ("Invalid type for value ")
                        : (!input.title && !input.value ?
                            ("Empty string") :
                            (input.title || input.value))
                    }
                </Dropdown.Item>
            )
        }) : (
                <Dropdown.Item
                    disabled
                >
                    No write value available
                </Dropdown.Item>
            );
        return (
            <div style={style}>
                {displayWidget && (
                    <span style={{ flexGrow: 0, marginRight: "0.5em" }}>{displayWidget}:</span>
                )}
                <Dropdown style={{ flexGrow: 1, marginRight: "0.5em" }} >
                    <Dropdown.Toggle
                        style={{
                            width: '100%',
                            ...dropdownButtonStyle
                        }}
                        variant="outline-secondary"
                        className="dropdown-toggle btn btn-sm"
                    >
                        {selectedValue ? selectedValue.title || selectedValue.value : dropdownTitle || "Dropdown"}
                    </Dropdown.Toggle>
                    <Dropdown.Menu
                        className="webjive-menu"
                    >
                        {selectedValue ?
                            <Dropdown.Item
                                key={-1}
                                as="button"
                                onClick={() => this.setState({ selectedValue: null })}
                            >
                                {/* {dropdownTitle || "Dropdown"} */}
                            Reset selection
                        </Dropdown.Item> : null
                        }
                        {values}
                    </Dropdown.Menu>
                </Dropdown>
                <Button
                    disabled={this.state.pending || selectedValue === null} 
                    onClick={() => this.handleSubmit()} 
                    style={submitButtonStyle} 
                    size="sm" 
                    variant="secondary" 
                    type="submit"
                    className="webjive-submit-btn"
                >
                    {submitButtonTitle || 'Submit'}
                </Button>
            </div>
        );
    }
}

const definition: WidgetDefinition<Inputs> = {
    type: "ATTRIBUTE WRITER DROPDOWN",
    name: "Attribute Writer Dropdown",
    defaultHeight: 10,
    defaultWidth: 14,
    inputs: {
        attribute: {
            type: "attribute",
            label: "",
            dataFormat: "scalar"
        },
        submitButtonTitle: {
            type: "string",
            label: "Button Title",
            placeholder: "Text on the button"
        },
        dropdownTitle: {
            type: "string",
            label: "Dropdown Title",
            placeholder: "Text on the dropdown"
        },
        writeValues: {
            label: "Write Values",
            type: "complex",
            repeat: true,
            inputs: {
                title: {
                    type: "string",
                    label: "Label",
                    placeholder: "Text on the option",
                },
                value: {
                    label: "Value",
                    type: "string",
                    placeholder: "Write value",
                }
            }
        },
        showDevice: {
            type: "boolean",
            label: "Show Device Name",
            default: true
        },
        showAttribute: {
            type: "select",
            label: "Attribute display:",
            default: "Label",
            options: [
                {
                    name: "Label",
                    value: "Label"
                },
                {
                    name: "Name",
                    value: "Name"
                },
                {
                    name: "None",
                    value: "None"
                }
            ]
        },
        textColor: {
            label: "Text Color",
            type: "color",
            default: "#000000"
        },
        backgroundColor: {
            label: "Background Color",
            type: "color",
            default: "#ffffff"
        },
        size: {
            label: "Text size (in units)",
            type: "number",
            default: 1,
            nonNegative: true
        },
        font: {
            type: "select",
            default: "Helvetica",
            label: "Font type",
            options: [
                {
                    name: "Default (Helvetica)",
                    value: "Helvetica"
                },
                {
                    name: "Monospaced (Courier new)",
                    value: "Courier new"
                }
            ]
        },
        dropdownButtonCss: {
            type: "style",
            label: "Dropdown Title CSS"
        },
        submitButtonCss: {
            type: "style",
            label: "Submit Button CSS"
        },
    }
};

export default {
    definition,
    component: AttributeWriterDropdown
};
