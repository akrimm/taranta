import React, { Component, ChangeEvent } from "react";
import TangoAPI from "../../shared/api/tangoAPI";
import { RootState } from "../state/reducers";
import { connect } from "react-redux";

import {
    WidgetDefinition,
    StringInputDefinition, 
    VariableInputDefinition,
    Dashboard,
    Variable,
    SelectedDashboard
} from "../types";
import { getDashboards, getSelectedDashboard } from "../state/selectors";
import { getDashboardVariables } from '../utils/DashboardVariables';
import { saveDashboard } from "../../dashboard/state/actionCreators";

type Inputs = {
  name: StringInputDefinition;
  variable: VariableInputDefinition;
};

type Props = {
  selectedDashboard: SelectedDashboard;
  dashboards: Dashboard[];
  tangoDB: string;
  inputs: Inputs;
  mode: string;
  saveDashboard: (id: string, name: string, widgets: any, variables: Variable[]) => void;
}

interface Device {
  name: string;
}

interface ParametricDropDown {
  devices: Device[],
  defaultDevice: string;
}

interface State {
  tangoClasses: any;
}

class Parametric extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);

    this.state = {
      tangoClasses: []
    }
  }

  componentDidMount() {
    this.fetchClasses();
  }

  /**
   * This function updated device change in mongoDB & redux store,
   * this will result in updating subscribed widgets
   */
  changeDevice(e: ChangeEvent<HTMLSelectElement>) {
    const variableName = this.props.inputs.variable.toString();
    let variables: Variable[] = getDashboardVariables(this.props.selectedDashboard.id, this.props.dashboards);

    variables = variables.map(variable => {
      if (variable.name === variableName)
        variable.device = e.target.value;
      return variable;
    })

    this.props.saveDashboard(this.props.selectedDashboard.id, this.props.selectedDashboard.name, Object.values(this.props.selectedDashboard.widgets), variables);
    this.forceUpdate();
  }

  public render() {
    const { inputs } = this.props;
    const { variable } = inputs;
    const name = inputs.name || inputs.variable
    const result: ParametricDropDown = this.getDevicesByClassName(variable);

    return (
      <div style={{padding: "0.25em 0.5em"}}>
        <span>{name}</span>
        <select className="form-control parametric-dropdown"
          style={{marginTop: "0px"}}
          value={result.defaultDevice}
          disabled={'library' === this.props.mode}
          onChange={(e) => this.changeDevice(e)}
        >
          {result.devices.length === 0 &&
            <option key='' disabled value=''>No device found</option>
          }
          {result.devices.length > 0 && result.devices.map((device) => {
            return <option key={device.name}>{device.name}</option>
          })}
        </select>
      </div>
    );
  }

  getDevicesByClassName(selectedVariableName: any) {
    const result: ParametricDropDown = {
      devices: [],
      defaultDevice: ''
    };

    const variables: Variable[] = getDashboardVariables(this.props.selectedDashboard.id, this.props.dashboards);
    const variable: Variable = variables.filter((variable) => (variable.name === selectedVariableName))[0];

    if (undefined !== variable) {
      const tangoClass = this.state.tangoClasses.find((c) => c.name === variable.class);

      if (undefined !== tangoClass && 0 < tangoClass.devices.length) {
        result.devices = tangoClass.devices;
        result.defaultDevice = variable.device;
      }
    }

    return result;
  }

  async fetchClasses() {
    const { tangoDB } = this.props;

    const data = await TangoAPI.fetchAllClassesAndDevices(tangoDB);
    this.setState({ tangoClasses: data })
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "PARAMETRIC_WIDGET",
  name: "Variable Selector",
  defaultHeight: 4,
  defaultWidth: 10,
  inputs: {
    name: {
      type: "string",
      label: "Name:",
      default: "Name",
      placeholder: "Name of variable",
      required: true
    },
    variable: {
      type: "variable",
      label: "Variable:",
    },
  } 
};

function mapStateToProps(state: RootState) {
  return {
    selectedDashboard: getSelectedDashboard(state),
    dashboards: getDashboards(state)
  };
}

function mapDispatchToProps(dispatch) {
  return {
    saveDashboard: (id: string, name: string, widgets: any, variables: Variable[]) => dispatch(saveDashboard(id, name, widgets, variables))
  }
}
export const PW = connect(
  mapStateToProps,
  mapDispatchToProps
)(Parametric);

export default {
  definition,
  component: PW
};
