import { WidgetProps } from "./types";
import { parseCss } from "../components/Inspector/StyleSelector";
import React, { Component, CSSProperties } from "react";
import {
  WidgetDefinition,
  CommandInputDefinition,
  StringInputDefinition,
  StyleInputDefinition,
  BooleanInputDefinition,
  ColorInputDefinition,
  NumberInputDefinition,
  SelectInputDefinition,
} from "../types";

import CommandArgsFile from "../../shared/components/CommandArgsFile/CommandArgsFile";

type Inputs = {
  title: StringInputDefinition;
  uploadBtnLabel: StringInputDefinition;
  buttonLabel: StringInputDefinition;
  command: CommandInputDefinition;
  showDevice: BooleanInputDefinition;
  showCommand: BooleanInputDefinition;
  requireConfirmation: BooleanInputDefinition;
  displayOutput: BooleanInputDefinition;
  textColor: ColorInputDefinition;
  backgroundColor: ColorInputDefinition;
  size: NumberInputDefinition;
  font: SelectInputDefinition;
  uploadButtonCss: StyleInputDefinition;
  sendButtonCss: StyleInputDefinition;
};

type Props = WidgetProps<Inputs>;

class CommandFile extends Component<Props> {

  public render() {
    const { mode, inputs } = this.props;
    const {
      title,
      uploadBtnLabel,
      buttonLabel,
      command,
      showDevice,
      showCommand,
      backgroundColor,
      textColor,
      size,
      font,
      uploadButtonCss,
      sendButtonCss,
      requireConfirmation
    } = inputs;

    const uploadBtnCss = parseCss(uploadButtonCss).data;
    const sendBtnCss = parseCss(sendButtonCss).data;
    const outerDivCss = {
      marginTop: "5px",
      marginBottom: "5px",
      backgroundColor: backgroundColor,
      color: textColor,
      fontSize: size,
      fontFamily: font,
    };

    const { device, command: commandName } = command;
    const deviceLabel = device || "device";
    const commandLabel = commandName || "command";
    const sendBtnText =
      "" === buttonLabel
        ? "Send " + (null !== commandName ? commandName : "")
        : buttonLabel;

    let label = [
      ...(showDevice ? [deviceLabel] : []),
      ...(showCommand ? [commandLabel] : []),
    ].join("/");

    label = (title !== undefined ? title : "") + " " + label + " ";

    const [output, outputStyle] = this.outputAndStyle();
    const fullOutputStyle: CSSProperties = {
      marginLeft: "0.5em",
      fontSize: "80%",
      ...outputStyle,
    };

    return (
      <div>
        <CommandArgsFile
          label={label}
          uploadBtnCss={uploadBtnCss}
          uploadBtnLabel={uploadBtnLabel}
          sendBtnCss={sendBtnCss}
          sendBtnText={sendBtnText}
          fullOutputStyle={fullOutputStyle}
          output={output}
          mode={mode}
          command={command}
          commandName={command.command}
          commandDevice={command.device}
          requireConfirmation={requireConfirmation}
          outerDivCss={outerDivCss}
        />
      </div>
    );
  }

  private outputAndStyle(): [string, CSSProperties] {
    const { mode, inputs } = this.props;
    const { command, displayOutput } = inputs;
    const { output } = command;

    const shownOutput = displayOutput
      ? mode === "run"
        ? output
        : "output"
      : "";

    if (mode !== "run") {
      return [shownOutput, { color: "gray", fontStyle: "italic" }];
    }

    if (displayOutput && output === undefined) {
      return ["n/a", { color: "gray" }];
    }

    return [shownOutput, {}];
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "command_file",
  name: "Command File",
  defaultWidth: 18,
  defaultHeight: 2,

  inputs: {
    title: {
      type: "string",
      label: "Title",
      placeholder: "Title of widget",
    },
    uploadBtnLabel: {
      type: "string",
      label: "Upload Button Text",
      default: "Upload File",
      placeholder: "Text for Upload Button",
    },
    buttonLabel: {
      type: "string",
      label: "Send Button Text",
      default: "",
      placeholder: "Text for Send Command",
    },
    command: {
      label: "Command",
      type: "command",
      intype: "NotDevVoid",
    },
    showDevice: {
      type: "boolean",
      label: "Show Device",
      default: false,
    },
    showCommand: {
      type: "boolean",
      label: "Show Command",
      default: false,
    },
    requireConfirmation: {
      type: "boolean",
      label: "Require Confirmation",
      default: true,
    },
    displayOutput: {
      type: "boolean",
      label: "Display Output",
      default: true,
    },
    textColor: {
      label: "Text Color",
      type: "color",
      default: "#000000",
    },
    backgroundColor: {
      label: "Background Color",
      type: "color",
      default: "#ffffff",
    },
    size: {
      label: "Text size (in units)",
      type: "number",
      default: 14,
      nonNegative: true,
    },
    font: {
      type: "select",
      default: "Helvetica",
      label: "Font type",
      options: [
        {
          name: "Default (Helvetica)",
          value: "Helvetica",
        },
        {
          name: "Monospaced (Courier new)",
          value: "Courier new",
        },
        {
          name: "Verdana",
          value: "Verdana",
        },
      ],
    },
    uploadButtonCss: {
      type: "style",
      label: "Upload Button CSS",
    },
    sendButtonCss: {
      type: "style",
      label: "Send Button CSS",
    },
  },
};

export default { component: CommandFile, definition };
