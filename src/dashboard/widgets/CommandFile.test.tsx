import React from "react";
import { CommandInput } from "../types";
import CommandFile from "./CommandFile";

import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

configure({ adapter: new Adapter() });

describe('upload-test', () => {
  let myCommandInput: CommandInput;

it('render widget with normal scenario', () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "output",
      execute: () => null
    };

    const element = React.createElement(CommandFile.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'Upload File',
        uploadBtnLabel: '',
        buttonLabel: 'Send Command',
        command: myCommandInput,
        showDevice: true,
        showCommand: true,
        requireConfirmation: true,
        displayOutput: true,
        textColor: '#000000',
        backgroundColor: '#ffffff',
        size: 14,
        font: 'Helvetica',
        uploadButtonCss: '',
        sendButtonCss: ''
      }
    });


    //check output for edit mode
    const shallowElement = mount(element);
    expect(shallowElement.find('.output-wrapper').text()).toContain('output');

    const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, '');
    expect(elemNoWhiteSpace).toContain("sys/tg_test/1/configureArray");
    expect(elemNoWhiteSpace).toContain("SendCommand");    

       
  });

  it('render widget with different setting', () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevVarStringArray",
      output: "",
      execute: () => null
    };
    let element = React.createElement(CommandFile.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'Upload File',
        uploadBtnLabel: '',
        buttonLabel: '',
        command: myCommandInput,
        showDevice: true,
        showCommand: true,
        requireConfirmation: true,
        displayOutput: true,
        textColor: '#000000',
        backgroundColor: '#ffffff',
        size: 14,
        font: 'Helvetica',
        uploadButtonCss: '',
        sendButtonCss: ''
      }
    });


    let shallowElement = mount(element);
    //Send button to have text as command name
    expect(shallowElement.find('.btn-send').text()).toContain('DevVarStringArray');
    expect(shallowElement.text()).toContain('sys/tg_test/1');

    element = React.createElement(CommandFile.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'Upload File',
        uploadBtnLabel: '',
        buttonLabel: 'SendMyCommand',
        command: myCommandInput,
        showDevice: false,
        showCommand: false,
        requireConfirmation: true,
        displayOutput: true,
        textColor: '#000000',
        backgroundColor: '#ffffff',
        size: 14,
        font: 'Helvetica',
        uploadButtonCss: '',
        sendButtonCss: ''
      }
    });

    shallowElement = mount(element);
    //same command with different settings
    expect(shallowElement.find('.btn-send').text()).not.toContain('DevVarStringArray');
    expect(shallowElement.text()).not.toContain('sys/tg_test/1');
    expect(shallowElement.find('.btn-send').text()).toContain('SendMyCommand');
   
  });

it('render widget with custom CSS', () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "sample output",
      execute: () => null
    };

    const element = React.createElement(CommandFile.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'My Title',
        uploadBtnLabel: 'Upload File',
        buttonLabel: 'Send Command',
        command: myCommandInput,
        showDevice: false,
        showCommand: false,
        requireConfirmation: true,
        displayOutput: false,
        textColor: '#000000',
        backgroundColor: '#ffffff',
        size: 14,
        font: 'Verdana',
        uploadButtonCss: 'border-radius:50%',
        sendButtonCss: 'border-radius:45%'
      }
    });

    const elemNoWhiteSpace = mount(element).html().replace(/\s/g, '');
    expect(elemNoWhiteSpace).toContain("UploadFile");
    expect(elemNoWhiteSpace).toContain("MyTitle");

    expect(elemNoWhiteSpace).toContain("font-size:14px");
    expect(elemNoWhiteSpace).toContain("font-family:Verdana");

    expect(elemNoWhiteSpace).not.toContain("sys/tg_test/1");
    expect(elemNoWhiteSpace).not.toContain("configureArray");
    
  });
});