import React from "react";
import {
  AttributeInput,
} from "../types";

import { configure, shallow, mount, render } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import attributeWriterDropDown from "./AttributeWriterDropDown";
import { act } from 'react-dom/test-utils';

interface Inputs {
  attribute: AttributeInput;
  dropdownTitle: string;
  submitButtonTitle: string;
  writeValues: { title: string, value: string }[];
  showDevice: boolean;
  showAttribute: string;
  textColor: string;
  backgroundColor: string;
  size: number;
  font: string;
  dropdownButtonCss: string;
  submitButtonCss: string;
}

configure({ adapter: new Adapter() });

describe("AttributeWriterDropdown", () => {
  const date = new Date();
  const timestamp = date.getTime();
  const initialAttributeInput: AttributeInput = {
    device: "",
    attribute: "",
    label: "",
    history: [],
    dataType: "",
    dataFormat: "",
    isNumeric: true,
    unit: "",
    enumlabels: [],
    write: () => { },
    value: "",
    writeValue: "",
    timestamp: timestamp
  }
  const initialInput: Inputs = {
    attribute: initialAttributeInput,
    dropdownTitle: "",
    submitButtonTitle: "",
    writeValues: [],
    showDevice: true,
    showAttribute: "Label",
    textColor: "",
    backgroundColor: "",
    size: 1,
    font: "Helvetica",
    dropdownButtonCss: "",
    submitButtonCss:""
  }

  it("renders an empty widget in edit/run mode without crashing", () => {
    const testInput = initialInput;
    const props = {
      t0: 1,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInput
    }
    const elementInEdit = React.createElement(attributeWriterDropDown.component, {
      mode: "edit",
      ...props
    });
    const elemInEditHtml = shallow(elementInEdit).html().replace(/\s/g, '');
    expect(elemInEditHtml).toContain("device/attributeLabel");
    expect(elemInEditHtml).toContain("Dropdown");
    expect(elemInEditHtml).toContain("color:");
    expect(elemInEditHtml).toContain("background-color:");
    expect(elemInEditHtml).toContain("font-size:1em");
    expect(elemInEditHtml).toContain("font-family:Helvetica");

    const elementInRun = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      ...props
    });
    const elemInRunHtml = shallow(elementInRun).html().replace(/\s/g, '');
    expect(elemInRunHtml).toContain("device/attributeLabel");
    expect(elemInRunHtml).toContain("Dropdown");
    expect(elemInRunHtml).toContain("color:");
    expect(elemInRunHtml).toContain("background-color:");
    expect(elemInRunHtml).toContain("font-size:1em");
    expect(elemInRunHtml).toContain("font-family:Helvetica");
  }); //renders an empty widget in edit/run mode without crashing

  it("renders widget with parameter in edit/run mode", () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: "sys/tg_test/1",
      attribute: "numeric",
      label: "test_numeric",
    }
    const testInput = {
      ...initialInput,
      attribute: testAttributeInput,
      dropdownTitle: "TestNumeric",
      submitButtonTitle: "TestSubmitButton",
      showDevice: true,
      showAttribute: "Label",
      textColor: "red",
      backgroundColor: "green",
      size: 2,
      font: "Courier new",
      dropdownButtonCss: "background-color: green",
      submitButtonCss: "background-color: blue"
    }
    const props = {
      t0: 1,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInput
    }
    const elementInEdit = React.createElement(attributeWriterDropDown.component, {
      mode: "edit",
      ...props
    });
    const elementInEditHtml = shallow(elementInEdit).html().replace(/\s/g, '');
    expect(elementInEditHtml).toContain("sys/tg_test/1/test_numeric");
    expect(elementInEditHtml).toContain("TestNumeric");
    expect(elementInEditHtml).toContain("TestSubmitButton");
    expect(elementInEditHtml).toContain("color:red");
    expect(elementInEditHtml).toContain("font-size:2em");
    expect(elementInEditHtml).toContain("font-family:Couriernew");
    expect(elementInEditHtml).toContain("background-color:blue");
    expect(elementInEditHtml).toContain("background-color:green");

    const elementInRun = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      ...props
    });
    const elementInRunHtml = shallow(elementInRun).html().replace(/\s/g, '');
    expect(elementInRunHtml).toContain("sys/tg_test/1/test_numeric");
    expect(elementInRunHtml).toContain("TestNumeric");
    expect(elementInRunHtml).toContain("TestSubmitButton");
    expect(elementInRunHtml).toContain("color:red");
    expect(elementInRunHtml).toContain("background-color:green");
    expect(elementInRunHtml).toContain("font-size:2em");
    expect(elementInRunHtml).toContain("font-family:Couriernew");
    expect(elementInRunHtml).toContain("background-color:blue");
  }); // renders widget with parameter in edit/run mode

  it("renders Dropdown with NO write value / with write values ", async () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: "sys/tg_test/1",
      attribute: "numeric",
      label: "test_numeric",
    }
    // No write values
    const testInputNowriteValue = {
      ...initialInput,
      writeValues: [],
      attribute: testAttributeInput,
      
    }
    const elementNoWriteValue = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      t0: 2,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInputNowriteValue
    });
    const componentNoWriteValue = mount(elementNoWriteValue);
    act(() => {
      expect(componentNoWriteValue.find('.dropdown').find('button').length).toEqual(1);
      componentNoWriteValue.find('.dropdown').find('button').simulate('click');
    })
    expect(componentNoWriteValue.html()).toContain("No write value available");
    componentNoWriteValue.unmount();

    //with write values
    const testInputWithWriteValues = {
      ...initialInput,
      writeValues: [{ title: 'highest', value: "10" }, { title: 'lowest', value: '1' }],
      attribute: testAttributeInput,
    }
    const elementWithWriteValues = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      t0: 2,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInputWithWriteValues
    });
    const componentWithWriteValues = mount(elementWithWriteValues);
    expect(componentWithWriteValues.find('.dropdown').find('button').length).toEqual(1);
    componentWithWriteValues.find('.dropdown').find('button').simulate('click');
    expect(componentWithWriteValues.html()).toContain("highest");
    expect(componentWithWriteValues.html()).toContain("write value: 10");
    expect(componentWithWriteValues.html()).toContain("lowest");
    expect(componentWithWriteValues.html()).toContain("write value: 1");
    componentWithWriteValues.unmount();
  }); // renders Dropdown with NO write value / with write values

  it("renders Dropdown with empty input on write values", () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: "sys/tg_test/1",
      attribute: "string",
      label: "test_string",
      dataType: "DevString",
      isNumeric: false
    }
    const testInput = {
      ...initialInput,
      writeValues: [{ title: '', value: '' }, { title: '', value: '' }],
      attribute: testAttributeInput,
    }
    const element = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      t0: 2,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInput
    });

    const component = mount(element);
    act(() => {
      expect(component.find('.dropdown').find('button').length).toEqual(1);
      component.find('.dropdown').find('button').simulate('click');
    })
    expect(component.html()).toContain("Empty string");
    expect(component.html()).toContain("Empty string");
    component.unmount();
  }); // renders Dropdown with empty input on write values

  it("renders Dropdown with data type boolean of write values", () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: "sys/tg_test/1",
      attribute: "boolean",
      label: "test_boolean",
      dataType: "DevBoolean",
      isNumeric: false
    }
    const testInput = {
      ...initialInput,
      writeValues: [{ title: 'On', value: 'true' }, { title: 'Off', value: 'false' }],
      attribute: testAttributeInput,
      showAttribute: "Name",
    }
    const element = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      t0: 2,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInput
    });

    const component = mount(element);
    expect(component.find('.dropdown').find('button').length).toEqual(1);
    component.find('.dropdown').find('button').simulate('click');
    const componentHtml = component.html();
    expect(componentHtml).toContain("sys/tg_test/1/boolean");
    expect(componentHtml).toContain("On");
    expect(componentHtml).toContain("true");
    expect(componentHtml).toContain("Off");
    expect(componentHtml).toContain("false");
    component.unmount();
  }); // renders Dropdown with data type boolean of write values

  it("renders Dropdown with data type other (not number, string or boolean) of write values", () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: "sys/tg_test/1",
      attribute: "other",
      label: "test_other_data_type",
      dataType: "DevState",
      isNumeric: false
    }
    const testInput = {
      ...initialInput,
      writeValues: [{ title: 'On', value: 'true' }, { title: 'Off', value: 'false' }],
      attribute: testAttributeInput,
      showAttribute: "Name",
    }
    const element = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      t0: 2,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInput
    });

    const component = mount(element);
    expect(component.html()).toContain("DevState not implemented");
    component.unmount();
  }); // renders Dropdown with data type other (not number, string or boolean) of write values

  it("renders Dropdown with wrong type for write value", () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: "sys/tg_test/1",
      attribute: "number",
      label: "test_number",
      isNumeric: true
    }
    const testInput = {
      ...initialInput,
      writeValues: [{ title: 'highest', value: 'highest' }],
      attribute: testAttributeInput,
      showDevice: false,
      showAttribute: "Name",
    }
    const element = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      t0: 2,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInput
    });

    const component = mount(element);
    const componentHtml = component.html();
    expect(componentHtml).not.toContain("sys/tg_test/1/number");
    component.find('.dropdown').find('button').simulate('click');
    expect(component.html()).toContain("Invalid type for value");
    component.unmount();
  }); // renders Dropdown with wrong type for write value

  it("test dropdownTitle change", () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: "sys/tg_test/1",
      attribute: "number",
      label: "test_number",
      isNumeric: true
    }
    const testInput = {
      ...initialInput,
      writeValues: [{ title: 'highest', value: '10' }, { title: 'lowest', value: '1' }],
      attribute: testAttributeInput,
    }
    const element = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      t0: 2,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInput
    });

    const component = mount(element);

    // Click on the dropdown button
    component.find('.dropdown').find('.dropdown-toggle').first().simulate('click');
    // Click on the second option in dropdown
    component.find('.webjive-menu').find('.dropdown-item').first().simulate('click');
    // expect toggle button to change title
    expect(component.find('.dropdown').find('.dropdown-toggle').first().html()).toContain('highest');

    // Test reset selection
    // Click on the dropdown button
    component.find('.dropdown').find('.dropdown-toggle').first().simulate('click');
    // Click on the Reset selection option in dropdown
    component.find('.webjive-menu').find('.dropdown-item').first().simulate('click');
    // expect toggle button to change title
    expect(component.find('.dropdown').find('.dropdown-toggle').first().html()).toContain('Dropdown');
    component.unmount();
  }); // test dropdownTitle change

  it("test handleSubmit", () => {
    const testAttributeInput = {
      ...initialAttributeInput,
      device: "sys/tg_test/1",
      attribute: "number",
      label: "test_number",
      isNumeric: true
    }
    const testInput = {
      ...initialInput,
      writeValues: [{ title: 'highest', value: '10' }, { title: 'lowest', value: '1' }],
      attribute: testAttributeInput,
    }
    const element = React.createElement(attributeWriterDropDown.component, {
      mode: "run",
      t0: 2,
      actualWidth: 20,
      actualHeight: 20,
      inputs: testInput
    });

    const component = mount(element);
    // Click on the dropdown button
    component.find('.dropdown').find('button').simulate('click');
    component.update();

    // Click on the first option in dropdown
    component.find('.webjive-menu').find('button').first().simulate('click');
    component.find('.webjive-submit-btn').find('button').simulate('click');
    
    expect(component.state()['pending']).toEqual(true);
  }); // test handleSubmit
});

