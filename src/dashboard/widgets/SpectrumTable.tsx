import React, { Component, CSSProperties } from "react";

import { WidgetProps } from "./types";
import {
  WidgetDefinition,
  BooleanInputDefinition,
  AttributeInputDefinition,
  SelectInputDefinition,
  NumberInputDefinition,
} from "../types";

// prettier-ignore
const sampleData = [10, 25, 38, 135, 9856];

type Inputs = {
  showDevice: BooleanInputDefinition;
  showAttribute: SelectInputDefinition;
  attribute: AttributeInputDefinition;
  precision: NumberInputDefinition;
  showIndex: BooleanInputDefinition;
  showLabel: BooleanInputDefinition;
  fontSize: NumberInputDefinition;
  layout: SelectInputDefinition<"horizontal" | "vertical">;
};

interface State {
  min?: number;
  max?: number;
}

type Props = WidgetProps<Inputs>;

function Table(props:Props) {
  
  const { mode, inputs } = props;
  const { attribute, showAttribute, precision} = inputs;

  let value = mode === "run" ? attribute.value : mode === "library" || mode === "edit" ? sampleData : [];
  value = value === undefined || value === null ? [null] : value;
  //if(value !== undefined) value = (Math.round(value * 100) / 100).toFixed(3);

  const tdStyle: CSSProperties = { marginLeft: "5px", padding: "0.5em", whiteSpace: "nowrap", border: "1px solid black", textAlign: "center" };
  const mainDivStyle: CSSProperties = { marginLeft: "5px", fontSize: inputs.fontSize+"px"};
  const spanStyle: CSSProperties = { marginLeft: "5px", display: "inline"};

  let spanText = inputs.showDevice === true ? attribute.device+"/" : "";

  let display = "";
    if(showAttribute === "Label") {
      if(attribute.label!=="") display = attribute.label;
      else display = attribute.attribute;
    } 
    else if(showAttribute === "Name"){
      if(attribute.attribute!==null) display = attribute.attribute;
      else display = "attributeName";
    } 

  spanText += display;

  let hasValue = true;
  if(value.length === 1)
  {
    if(value[0] === null) hasValue = false;
  }

  return (
    <div style={mainDivStyle}>
      { inputs.showDevice || inputs.showAttribute ? <span style={spanStyle}>{spanText}</span> : null }
      <div>
        {!hasValue && "No data" }
        {hasValue && 
        <table>
          { 
            inputs.layout === 'horizontal' ? 
          <tbody>{inputs.showIndex === true ? <tr>{inputs.showLabel === true ? <td style={tdStyle}>Index:</td> : null}
          {value.map((item, i) => { return [<td style={tdStyle}  key={i}>{i}</td>];})}</tr> : null}
            <tr>{inputs.showLabel === true ? <td style={tdStyle}>Value:</td> : null}
            {value.map((item, i) => { return [<th style={tdStyle}  key={i}>{item ? getFormattedValue(item,precision): item}</th>];})}</tr></tbody>
            : //vertical
            <tbody>
            {inputs.showLabel === true ? <tr>{inputs.showIndex === true ? <td style={tdStyle}>Index:</td>: null}
            <td style={tdStyle}>Value:</td></tr> : null}
            {value.map((item, i) => { return [<tr key={i}>{inputs.showIndex === true ? <td style={tdStyle}>{i}</td> : null}
            <th style={tdStyle}>{item!==null? getFormattedValue(item,precision): item}</th></tr>];})}</tbody>
          }
        </table>
        }
      </div>
    </div>
  );
  
}

function getFormattedValue(value, precision) {
  return (Number.isInteger(value) ? value : value.toFixed(precision));
}

class SpectrumTable extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = {};
  }

  public render() {
    return (
      <Table {...this.props}></Table>
    );
  }
}

const definition: WidgetDefinition<Inputs> = {
  type: "SPECTRUM_TABLE",
  name: "SpectrumTable",
  defaultWidth: 10,
  defaultHeight: 3,
  inputs: {
    attribute: {
      label: "",
      type: "attribute",
      dataFormat: "spectrum",
      dataType: "numeric",
      required: true
    },
    precision: {
      type: "number",
      label: "Precision",
      default: 3
    },
    layout: {
      type: "select",
      label: "Layout",
      default: "horizontal",
      options: [
        {
          name: "Horizontal",
          value: "horizontal"
        },
        {
          name: "Vertical",
          value: "vertical"
        }
      ]
    },
    showDevice: {
      type: "boolean",
      label: "Show Device",
      default: false
    },
    showAttribute: {
      type: "select",
      label: "Attribute display:",
      default: "Label",
      options: [
        {
          name: "Label",
          value: "Label"
        },
        {
          name: "Name",
          value: "Name"
        },
        {
          name: "None",
          value: "None"
        }
      ]
    },
    showIndex: {
      type: "boolean",
      label: "Show Index",
      default: false
    },
    showLabel: {
      type: "boolean",
      label: "Show Labels",
      default: false
    },
    fontSize: {
      type: "number",
      label: "Font Size (px)",
      default: 16,
      nonNegative: true
    },
  }
};

export default { component: SpectrumTable, definition };
