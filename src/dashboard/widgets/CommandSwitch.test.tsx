import React from "react";
import { CommandInput } from "../types";
import CommandSwitch from "./CommandSwitch";

import Enzyme, { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";

Enzyme.configure({ adapter: new Adapter() });

describe('switch-test', () => {
  let myCommandInput: CommandInput;

  it('render switch with normal scenario', () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "",
      execute: () => null
    };

    const element = React.createElement(CommandSwitch.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'Master Switch',
        onCommand: myCommandInput,
        offCommand: myCommandInput,
        showDevice: true,
        showCommand: true,
        onStateArgs: '',
        offStateArgs: '',
        onStateImageUrl: '',
        offStateImageUrl: '',
        onStateCss: 'background-color:green',
        offStateCss: 'background-color:red',
        imageCss: '',
        displayOutput: true,
        timeDisplayOutput: 1
      }
    });

    const elemNoWhiteSpace = shallow(element).html().replace(/\s/g, '');
    expect(elemNoWhiteSpace).toContain("sys/tg_test/1/configureArray");
    expect(elemNoWhiteSpace).toContain("MasterSwitch");
  });


  it('test autoclose popup', () => {
    jest.useFakeTimers()

    myCommandInput = {
      device: "sys/tg_test/1",
      command: "configureArray",
      output: "",
      execute: () => null
    };

    const element = React.createElement(CommandSwitch.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'Master Switch',
        onCommand: myCommandInput,
        offCommand: myCommandInput,
        showDevice: true,
        showCommand: true,
        onStateArgs: '',
        offStateArgs: '',
        onStateImageUrl: '',
        offStateImageUrl: '',
        onStateCss: 'background-color:green',
        offStateCss: 'background-color:red',
        imageCss: '',
        displayOutput: true,
        timeDisplayOutput: 1
      }
    });

    const shallowElement = shallow(element);
 
    shallowElement.state()['pending'] = false;
    element.props.inputs.onStateArgs = '';
    element.props.inputs.offStateArgs = '';
    
    shallowElement.find('.attribute-checkbox').simulate('change');
    jest.advanceTimersByTime(6000);
    expect(shallowElement.state()['pending']).toBe(false);
    
  });

  

it('render switch with different input types', () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "",
      execute: () => null
    };

    const element = React.createElement(CommandSwitch.component, {
      mode: "edit",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'Master Switch',
        onCommand: myCommandInput,
        offCommand: myCommandInput,
        showDevice: true,
        showCommand: true,
        onStateArgs: '',
        offStateArgs: '',
        onStateImageUrl: '',
        offStateImageUrl: '',
        onStateCss: 'background-color:green',
        offStateCss: 'background-color:red',
        imageCss: '',
        displayOutput: true,
        timeDisplayOutput: 1
      }
    });

    const shallowElement = shallow(element);
 
    shallowElement.state()['pending'] = false;
    element.props.inputs.onStateArgs = '';
    element.props.inputs.offStateArgs = '';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onStateArgs = 'true';
    element.props.inputs.offStateArgs = 'true';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onStateArgs = 'false';
    element.props.inputs.offStateArgs = 'false';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onStateArgs = '123';
    element.props.inputs.offStateArgs = '123';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onStateArgs = '11.11';
    element.props.inputs.offStateArgs = '11.11';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onCommand['acceptedType'] = 'DevVarStringArray';
    element.props.inputs.offCommand['acceptedType'] = 'DevVarStringArray';
    element.props.inputs.onStateArgs = '[1,2,3]';
    element.props.inputs.offStateArgs = '[1,2,3]';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onCommand['acceptedType'] = 'DevString';
    element.props.inputs.offCommand['acceptedType'] = 'DevString';
    element.props.inputs.onStateArgs = '[1,2,3]';
    element.props.inputs.offStateArgs = '[1,2,3]';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onStateArgs = '{1,2,3}';
    element.props.inputs.offStateArgs = '{1,2,3}';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onStateArgs = '"input"';
    element.props.inputs.offStateArgs = '"input"';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onCommand['acceptedType'] = 'DevUCharArray';
    element.props.inputs.offCommand['acceptedType'] = 'DevUCharArray';
    element.props.inputs.onStateArgs = '[1,2]';
    element.props.inputs.offStateArgs = '[3,4]';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onCommand['acceptedType'] = 'DevArray';
    element.props.inputs.offCommand['acceptedType'] = 'DevArray';
    element.props.inputs.onStateArgs = '[1,2]';
    element.props.inputs.offStateArgs = '[3,4]';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onCommand['acceptedType'] = 'DevVarStringArray';
    element.props.inputs.offCommand['acceptedType'] = 'DevVarStringArray';
    element.props.inputs.onStateArgs = '[1]';
    element.props.inputs.offStateArgs = '[2]';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onCommand['acceptedType'] = 'DevArray';
    element.props.inputs.offCommand['acceptedType'] = 'DevArray';
    element.props.inputs.onStateArgs = '[1]';
    element.props.inputs.offStateArgs = '[2]';
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.onCommand['output'] = undefined;
    element.props.inputs.offCommand['output'] = undefined;
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.displayOutput = false;
    element.props.inputs.onCommand['output'] = '1';
    element.props.inputs.offCommand['output'] = '1';
    shallowElement.find('.attribute-checkbox').simulate('change');

  });

  

it('render switch with custom image', () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "",
      execute: () => null
    };

    const element = React.createElement(CommandSwitch.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'Master Switch',
        onCommand: myCommandInput,
        offCommand: myCommandInput,
        showDevice: true,
        showCommand: true,
        onStateArgs: '',
        offStateArgs: '',
        onStateImageUrl: 'https://picsum.photos/200/300',
        offStateImageUrl: 'https://www.dumpaday.com/wp-content/uploads/2020/06/00-100-750x280.jpg',
        onStateCss: 'background-color:green',
        offStateCss: 'background-color:red',
        imageCss: 'border-radius:50%',
        displayOutput: true,
        timeDisplayOutput: 1
      }
    });

    const shallowElement = shallow(element);
    shallowElement.state()['pending'] = true;
    shallowElement.find('.attribute-checkbox').simulate('change');

    shallowElement.state()['pending'] = false;
    shallowElement.find('.attribute-checkbox').simulate('change');

    element.props.inputs.displayOutput = false;
    element.props.inputs.onCommand['output'] = '1';
    element.props.inputs.offCommand['output'] = '1';
    shallowElement.find('.attribute-checkbox').simulate('change');

    shallowElement.state()['pending'] = false;
    shallowElement.find('.attribute-checkbox').simulate('change');
    shallowElement.find('.attribute-checkbox').simulate('change');

    const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, '');
    expect(elemNoWhiteSpace).toContain("sys/tg_test/1/DevString");
    expect(elemNoWhiteSpace).toContain("border-radius:50%");
    
  });

  
it('check click event of switch', () => {
    myCommandInput = {
      device: "sys/tg_test/1",
      command: "DevString",
      output: "",
      execute: () => null
    };

    const element = React.createElement(CommandSwitch.component, {
      mode: "run",
      t0: 1,
      actualWidth: 100,
      actualHeight: 100,
      inputs: {
        title: 'Master Switch',
        onCommand: myCommandInput,
        offCommand: myCommandInput,
        showDevice: true,
        showCommand: true,
        onStateArgs: '',
        offStateArgs: '',
        onStateImageUrl: 'https://picsum.photos/200/300',
        offStateImageUrl: 'https://www.dumpaday.com/wp-content/uploads/2020/06/00-100-750x280.jpg',
        onStateCss: 'background-color:green',
        offStateCss: 'background-color:red',
        imageCss: 'border-radius:50%',
        displayOutput: true,
        timeDisplayOutput: 1
      }
    });

    //Trigger the click event on checkbox & confirm if image changes on switch
    const shallowElement = shallow(element);
    shallowElement.find('.attribute-checkbox').simulate('change');
    const checkboxState = shallowElement.state()['checkbox'];
    const elemNoWhiteSpace = shallowElement.html().replace(/\s/g, '');

    if (checkboxState) {
      expect(elemNoWhiteSpace).toContain("background-color:green");
      expect(elemNoWhiteSpace).toContain("https://picsum.photos/200/300");
    } else {
      expect(elemNoWhiteSpace).toContain("background-color:red");
      expect(elemNoWhiteSpace).toContain("https://www.dumpaday.com/wp-content/uploads/2020/06/00-100-750x280.jpg");
    }

    expect(elemNoWhiteSpace).toContain("sys/tg_test/1/DevString");
    expect(elemNoWhiteSpace).toContain("border-radius:50%");
  });

});
