import { Dashboard, Widget, Variable } from "./../types";

export const getDashboardVariables = (selectedDashboardId: string, dashboards: Dashboard[]) => {
  let variables: Variable[] = [];
  if (undefined === selectedDashboardId || '' === selectedDashboardId || 0 === dashboards.length) {
    return variables;
  }

  for (let i=0; i<dashboards.length; i++) {
    //Match the dashboard
    if (dashboards[i].id === selectedDashboardId && undefined !== dashboards[i].variables) {
      variables = dashboards[i].variables;
      break;
    }
  }
  return variables;
}

export const mapVariableNameToDevice = (widgets: Widget[], selectedDashboardId: string, dashboards: Dashboard[]) => {
  if (0 === widgets.length || 0 === selectedDashboardId.length || 0 === dashboards.length) {
    return widgets;
  }

  const parametricWidget: Widget[] = widgets.filter((w) => { return w.type === "PARAMETRIC_WIDGET"})
  const dashboardVariables = getDashboardVariables(selectedDashboardId, dashboards);
  //process only widget different from PARAMETRIC_WIDGET
  let strWidgets = typeof widgets === 'string' ? widgets : JSON.stringify(widgets.filter((w) => { return w.type !== "PARAMETRIC_WIDGET"}));

  for(let i=0; i<dashboardVariables.length; i++) {
    const expression = new RegExp(dashboardVariables[i].name, "g");
    strWidgets = strWidgets.replace(expression, dashboardVariables[i].device);
  }

  widgets = typeof widgets === 'string' ? strWidgets : JSON.parse(strWidgets);

  //add the original PARAMETRIC_WIDGET
  widgets.push(...parametricWidget);  

  return widgets.sort((a, b) => { return Number(b.id) - Number(a.id)});
}

export const getDeviceOfDashboardVariable = (variables: Variable[], variableName: string) => {
  let deviceName = variableName;

  for (let i=0; i<variables.length; i++) {
    //Match the variable
    if (variables[i].name === variableName) {
      deviceName = variables[i].device;
    }
  }

  return deviceName;
}

/**
 * This function checks if variables used in widget are defined in configuration
 *
 * @param widget
 * @param variables
 */
export const variablePresent = (widget: Widget, variables: Variable[]) => {
  let response = {
    found: true,
    device: ''
  }
 
  if(!variables) return response;
  if(widget.inputs.attribute && widget.inputs.attribute.device && -1 === widget.inputs.attribute.device.indexOf("/")){
    const variable = variables.find(variable => variable.name === widget.inputs.attribute.device);
    response.found = variable ? true : false;
    response.device = widget.inputs.attribute.device;

  } else if(widget.inputs.command && widget.inputs.command.device && -1 === widget.inputs.command.device.indexOf("/")){
    const variable = variables.find(variable => variable.name === widget.inputs.command.device);
    response.found = variable ? true : false;
    response.device = widget.inputs.command.device;

  } else if(widget.inputs.device && -1 === widget.inputs.device.indexOf("/")){
    const variable = variables.find(variable => variable.name === widget.inputs.device);
    response.found = variable ? true : false;
    response.device = widget.inputs.device;

  } else if(widget.inputs.attributes && 0 < widget.inputs.attributes.length){
    for (const attr of widget.inputs.attributes) {
      if(attr.attribute.device && -1 === attr.attribute.device.indexOf("/")){
        const variable = variables.find(variable => variable.name === attr.attribute.device);
        if (!variable) {
          response.found = false;
          response.device = attr.attribute.device;
          break;
        }
      }
    }
  }

  return response;
}


export function getRunningClasses(tangoClasses){
  //return the classes that has at least 1 device running
  return tangoClasses.filter(c => c.devices.filter(d => d.connected === true).length > 0)
}

export function   getRunningDevices(tangoClasses, TangoClass){
  //return the devices running for a Tango Class
  let data = tangoClasses.filter(c => c.name === TangoClass)[0]
  if(data !== undefined)
    return data.devices.filter(d => d.connected === true)
  else
    return []
}

export function checkVariableDevice(tangoDevice, tangoClasses){
  let status = {connected: false, exported: false}

  //it checks only when all classes are fetched
  if(tangoClasses.length > 0 )
  {
    let devices = tangoClasses.filter(c => c.devices.filter(d => d.name === tangoDevice).length > 0);
    if(devices.length>0)
    {
      let deviceStatus = devices[0].devices.filter(d => d.name === tangoDevice)[0]

      status.connected = deviceStatus.connected
      status.exported = deviceStatus.exported
    }
    
  }
  
  return status;
}

export function checkVariableConsistency(variables, tangoClasses){
  let valid = true; 
  if(variables !== undefined)
  {
    variables.forEach(variable => {
      let deviceRegistred = false;
      tangoClasses.forEach(tangoClass => {
        let devicesRunning = tangoClass["devices"]
        let deviceInVariable = devicesRunning.find( device => {
          return device.name === variable.device
        })
        if(deviceInVariable !== undefined)
        {
          deviceRegistred = true
          if(!deviceInVariable.exported || !deviceInVariable.connected)
            valid = false
        }
      })
      if(!deviceRegistred) 
        valid = false
    })
  }
  return valid
}