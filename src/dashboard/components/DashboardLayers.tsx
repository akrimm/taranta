import React, { Component } from "react";
import { Widget, Dashboard } from "../types";
import { connect } from "react-redux";
import { RootState } from "../state/reducers";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./DashboardLayers.css";
import { Dispatch } from "redux";
import {
  selectWidgets,
  reorderWidgets,
  deleteWidget,
} from "../state/actionCreators";
import {
  getWidgets,
  getSelectedDashboard,
  getSelectedWidgets,
} from "../state/selectors";
import LayerList from "./LayerList";

interface Props {
  selectedDashboard: Dashboard;
  widgets: Widget[];
  selectedWidgets: Widget[];
  onSelectWidgets: (ids: string[]) => void;
  onReorderWidget: (widgets: Widget[]) => void;
  onDeleteWidget: () => void;
  render: boolean;
}
class DashboardLayers extends Component<Props> {
  public render() {
    const {
      widgets,
      selectedDashboard,
      selectedWidgets,
      onSelectWidgets,
      onReorderWidget,
    } = this.props;
    const displayStyle = this.props.render ? "block" : "none";
    return (
      <div style={{ display: displayStyle }} tabIndex={0}>
        <LayerList
          selectedIds={selectedDashboard.selectedIds}
          widgets={widgets.sort((a, b) => b.order - a.order)}
          selectedWidgets={selectedWidgets}
          onSelectWidgets={onSelectWidgets}
          onReorderWidget={onReorderWidget}
        />
        <button
          title="Delete selected layers"
          onClick={this.props.onDeleteWidget}
          disabled={selectedDashboard.selectedIds.length === 0}
          className="btn btn-outline-secondary btn-sm btn-layer-action"
        >
          <FontAwesomeIcon icon="trash" />{" "}
        </button>
        <button
          title="Group selected layers"
          onClick={() => window.alert("not implemented yet")}
          disabled={selectedDashboard.selectedIds.length < 2}
          className="btn btn-outline-secondary btn-sm btn-layer-action"
        >
          <FontAwesomeIcon icon="layer-group" />{" "}
        </button>
      </div>
    );
  }
}

function mapStateToProps(state: RootState) {
  return {
    selectedDashboard: getSelectedDashboard(state),
    widgets: getWidgets(state),
    selectedWidgets: getSelectedWidgets(state),
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onSelectWidgets: (ids: string[]) => dispatch(selectWidgets(ids)),
    onReorderWidget: (widgets: Widget[]) => dispatch(reorderWidgets(widgets)),
    onDeleteWidget: () => dispatch(deleteWidget()),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(DashboardLayers);
