import React, { Component } from "react";
import TangoAPI from "../../shared/api/tangoAPI";
import alphanumSort from "alphanum-sort";

interface Props {
  tangoDB: string;
  variables: any;
}

interface State {
  fetching: boolean;
  error: boolean;
  devices: string[];
  onlyDevices: string[];
  tangoDB: string;
}

const initialState: Readonly<State> = {
  fetching: false,
  error: false,
  devices: [],
  onlyDevices: [],
  tangoDB: ""
};

const deviceContext = React.createContext<State>({ ...initialState });

export class DeviceProvider extends Component<Props, State> {
  public constructor(props: Props) {
    super(props);
    this.state = { ...initialState, tangoDB: props.tangoDB };
  }

  public componentDidUpdate(prevProps) {
    //Add newly added variables to the device list
    if (prevProps.variables.length !== this.props.variables.length)
      this.setState({ devices: [...this.props.variables , ...alphanumSort(this.state.onlyDevices)]});
  }

  public async componentDidMount() {
    this.setState({ fetching: true });
    const { tangoDB } = this.props;

    try {
      let devices = await TangoAPI.fetchDeviceNames(tangoDB);
      const error = devices.length === 0;
      devices = [...this.props.variables, ...alphanumSort(devices)];
      this.setState({ devices: devices, fetching: false, error, onlyDevices: devices });

    } catch (err) {
      this.setState({ fetching: false, error: true });
    } finally {
      if (this.state.error) console.log("Couldn't fetch devices from database",tangoDB);
        //feedBackService.setData({level: NotificationLevel.ERROR, message: `⚠️ Couldn't fetch devices from database ${tangoDB}.`})
    }
  }

  public render() {
    if (this.state.fetching){
      return <>Loading Devices</> ;
    }
    return (
      <deviceContext.Provider value={this.state}>
        {this.props.children}
      </deviceContext.Provider>
    );
  }
}

export const DeviceConsumer = deviceContext.Consumer;
