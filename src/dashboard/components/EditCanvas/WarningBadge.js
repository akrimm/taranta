import React from "react";

export default function WarningBadge({ visible, warningMessage }) {
  const radius = 10;
  return (
    <div
      className="WarningBadge"
      style={{
        position: "absolute",
        marginLeft: -radius,
        marginTop: warningMessage ? -radius * 2 : -radius,
        borderRadius: warningMessage ? "5px" : radius,
        width: warningMessage ? "auto" : 2 * radius,
        height: warningMessage ? "auto" : 2 * radius,
        backgroundColor: "#f2dede",
        borderColor: "#ebccd1",
        color: "#5C2524",
        textAlign: "center",
        opacity: visible ? 1 : 0,
        pointerEvents: "none",
        zIndex: 1,
        lineHeight: warningMessage ? "initial" : "",
        padding: warningMessage ? "5px" : "",
      }}
    >
      <span className="fa fa-exclamation" />
      {
        warningMessage ?
          <span style={{
            marginLeft: "3px",
            backgroundColor: "#f2dede",
            borderColor: "#ebccd1",
            color: "#5C2524",
          }}>
            {warningMessage}</span> : null}
    </div>
  );
}
