import reducers from './index';

const example_canvas = {
    '0': { id: '0', name: 'Root' },
    '1': { id: '1', name: 'Subcanvas 1' },
    '2': { id: '2', name: 'Subcanvas 2' },
    '3': { id: '3', name: 'Subcanvas 3' }
}

const example_widget = {
    '1':
    {
        _id: '5ef60bf206f0f400173d387e', id: '1', x: 9, y: 8, canvas: '0', width: 10, height: 2, type: 'ATTRIBUTE_DISPLAY',
        inputs: {
            attribute: { device: 'sys/tg_test/1', attribute: 'double_scalar', label: 'double_scalar' },
            precision: 2, showDevice: false, showAttribute: 'Label', scientificNotation: false,
            showEnumLabels: false, textColor: '#000000', backgroundColor: '#ffffff', size: 1, font: 'Helvetica'
        },
        order: 0, valid: true
    }
}

const example_user = {
    awaitingResponse: false, loginFailed: false, loginDialogVisible: false, username: 'TEST', userGroups: ['group3'],
    stateUrls: {deviceUrl: '/test/devices', dashboardUrl: '/test/dashboard'},
    notifications: []
};

const example_selectedDashboard = {
    selectedId: null, selectedIds: [], widgets: example_widget, id: '5ef1d12a2772460017319cec', name: 'Dash1',
    user: 'TEST', group: 'GROUP1', groupWriteAccess: false, lastUpdatedBy: 'TEST', insertTime: '2020-06-23T09:53:46.993Z',
    updateTime: '2020-06-26T14:53:38.373Z', history: {
        undoActions: [], redoActions: [], undoIndex: 0, redoIndex: 0,
        undoLength: 0, redoLength: 0
    }
}

const example_dashboard = {
    id: '5ef1d12a2772460017319cec',
    name: 'Dash1', user: 'TEST', insertTime: '2020-06-23T09:53:46.993Z',
    updateTime: '2020-06-26T14:53:38.373Z', groupWriteAccess: false, group: 'GROUP1', lastUpdatedBy: 'TEST', tangoDB: 'testdb'
}

const example_notification = { level: '', sourceAction: '', msg: '' };
const example_stateUrl = {
    deviceUrl: '',
    dashboardUrl: ''
}

test('reducers test OPEN_LOGIN_DIALOG', () => {

    let state = reducers({
        ui: { mode: 'edit', selectedCanvas: '0' },
        canvases: example_canvas, selectedDashboard: example_selectedDashboard, user: example_user,
        dashboards: { dashboards: [] }, notifications: { notification:  example_notification},
        clipboard: { widgets: [], pasteCounter: 0 }
    }, { 
        type: 'OPEN_LOGIN_DIALOG' 
    });

    expect(state).toEqual({
        ui: { mode: 'edit', selectedCanvas: '0' },
        canvases: example_canvas, selectedDashboard: example_selectedDashboard,
        user: { awaitingNotification: false, awaitingResponse: false, loginFailed: false, loginDialogVisible: true, notificationFailed:false, notifications: [],  stateUrls: example_stateUrl},
        dashboards: { dashboards: [] }, notifications: { notification: example_notification },
        clipboard: { widgets: [], pasteCounter: 0 }
    });
});

test('reducers test TOGGLE_MODE', () => {

    let state = reducers({
        ui: { mode: 'edit', selectedCanvas: '0' },
        canvases: example_canvas, selectedDashboard: example_selectedDashboard,
        user: example_user, dashboards: { dashboards: [example_dashboard] }, notifications: { 
            notification: example_notification },
        clipboard: { widgets: [], pasteCounter: 0 }
    }, { 
        type: 'TOGGLE_MODE' 
    });

    expect(state).toEqual({
        ui: { mode: 'run', selectedCanvas: '0' },
        canvases: example_canvas, selectedDashboard: example_selectedDashboard, user: example_user, 
        dashboards: { dashboards: [example_dashboard] }, notifications: { notification: example_notification },
        clipboard: { widgets: [], pasteCounter: 0 }
    });
});

test('reducers test DASHBOARD_LOADED', () => {

    let state = reducers({
        ui: { mode: 'edit', selectedCanvas: '0' }, canvases: example_canvas, selectedDashboard: example_selectedDashboard,
        user: example_user,
        dashboards: { dashboards: [example_dashboard] }, notifications: { notification: example_notification },
        clipboard: { widgets: [], pasteCounter: 0 }
    }, {
        type: 'DASHBOARD_LOADED', dashboard: example_dashboard, widgets: [example_widget['1']]
    });

    expect(state).toEqual({
        ui: { mode: 'edit', selectedCanvas: '0' }, 
        canvases: example_canvas, selectedDashboard: example_selectedDashboard, 
        user: example_user, dashboards: { dashboards: [example_dashboard] }, notifications: { notification: example_notification }, 
        clipboard: { widgets: [], pasteCounter: 0 }
    });
});
